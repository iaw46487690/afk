// Tipo de usuario y nombre de cuenta admin
var sessionValue = $("#typeSession").data('value');
var nameUser = $("#nameUser").data('value');
// Selects
var juego = document.getElementById('juego');
var plataforma = document.getElementById('plataforma');
var capacidad = document.getElementById('capacidad');
var modalidad = document.getElementById('modalidad');
var individual = document.getElementById('individual');
var equipo = document.getElementById('equipo');
// Juegos
var brawlStars = document.createElement("option");
brawlStars.text = "Brawl Stars";
var clashRoyale = document.createElement("option");
clashRoyale.text = "Clash Royale";
var csgo = document.createElement("option");
csgo.text = "CS:GO";
var fifa = document.createElement("option");
fifa.text = "FIFA";
var hearthstone = document.createElement("option");
hearthstone.text = "Hearthstone";
var lol = document.createElement("option");
lol.text = "LOL";
var marioKart = document.createElement("option");
marioKart.text = "Mario Kart";
var nba = document.createElement("option");
nba.text = "NBA 2K";
var overwatch = document.createElement("option");
overwatch.text = "Overwatch";
var pokemon = document.createElement("option");
pokemon.text = "Pokemon";
var rainbow = document.createElement("option");
rainbow.text = "Rainbow Six Siege";
var rocket = document.createElement("option");
rocket.text = "Rocket League";
var smash = document.createElement("option");
smash.text = "Super Smash Bros";
var valorant = document.createElement("option");
valorant.text = "Valorant";
// Plataformas
var playstation = document.createElement("option");
playstation.text = "PlayStation";
var xbox = document.createElement("option");
xbox.text = "XBOX";
var nintendoSwitch = document.createElement("option");
nintendoSwitch.text = "Nintendo Switch";
var pc = document.createElement("option");
pc.text = "PC";
var movil = document.createElement("option");
movil.text = "Movil";
// Capacidad
var option8 = document.createElement("option");
option8.text = "8 participantes";
option8.value = 8;
var option16 = document.createElement("option");
option16.text = "16 participantes";
option16.value = 16;
var option32 = document.createElement("option");
option32.text = "32 participantes";
option32.value = 32;
var option64 = document.createElement("option");
option64.text = "64 participantes";
option64.value = 64;
// Modalidad
var option2 = document.createElement("option");
option2.text = "Equipos de 2 jugadores";
option2.value = 2;
var option3 = document.createElement("option");
option3.text = "Equipos de 3 jugadores";
option3.value = 3;
var option4 = document.createElement("option");
option4.text = "Equipos de 4 jugadores";
option4.value = 4;
var option5 = document.createElement("option");
option5.text = "Equipos de 5 jugadores";
option5.value = 5;
var option6 = document.createElement("option");
option6.text = "Equipos de 6 jugadores";
option6.value = 6;
if (sessionValue == "estandar") {
    juego.add(brawlStars);
    juego.add(clashRoyale);
    juego.add(csgo);
    juego.add(fifa);
    juego.add(hearthstone);
    juego.add(lol);
    juego.add(marioKart);
    juego.add(nba);
    juego.add(overwatch);
    juego.add(pokemon);
    juego.add(rainbow);
    juego.add(rocket);
    juego.add(smash);
    juego.add(valorant);
    function validacio() {
        if (juego.value == "Brawl Stars") {
            removeOptions();
            plataforma.add(movil);
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option2);
            individual.disabled = false;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = false;
            ocultar();
        } else if (juego.value == "Clash Royale") {
            removeOptions();
            plataforma.add(movil);
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option2);
            individual.disabled = false;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = false;
            ocultar();
        } else if (juego.value == "CS:GO") {
            removeOptions();
            plataforma.add(pc);
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option5);
            individual.disabled = false;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = false;
            ocultar();
        } else if (juego.value == "FIFA") {
            removeOptions();
            plataforma.add(playstation);
            plataforma.add(xbox);
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            individual.disabled = false;
            equipo.disabled = true;
            individual.checked = true;
            equipo.checked = false;
            ocultar();
        } else if (juego.value == "Hearthstone") {
            removeOptions();
            plataforma.add(pc);
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            individual.disabled = false;
            equipo.disabled = true;
            individual.checked = true;
            equipo.checked = false;
            ocultar();
        } else if (juego.value == "LOL") {
            removeOptions();
            plataforma.add(pc);
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option5);
            individual.disabled = true;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = true;
            mostrar();
        } else if (juego.value == "Mario Kart") {
            removeOptions();
            plataforma.add(nintendoSwitch);
            capacidad.add(option8);
            individual.disabled = false;
            equipo.disabled = true;
            individual.checked = true;
            equipo.checked = false;
            ocultar();
        } else if (juego.value == "NBA 2K") {
            removeOptions();
            plataforma.add(playstation);
            plataforma.add(xbox);
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option3);
            modalidad.add(option5);
            individual.disabled = true;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = true;
            mostrar();
        } else if (juego.value == "Overwatch") {
            removeOptions();
            plataforma.add(playstation);
            plataforma.add(xbox);
            plataforma.add(pc);
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option6);
            individual.disabled = true;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = true;
            mostrar();
        } else if (juego.value == "Pokemon") {
            removeOptions();
            plataforma.add(nintendoSwitch);
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            individual.disabled = false;
            equipo.disabled = true;
            individual.checked = true;
            equipo.checked = false;
            ocultar();
        } else if (juego.value == "Rainbow Six Siege") {
            removeOptions();
            plataforma.add(playstation);
            plataforma.add(xbox);
            plataforma.add(pc);
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option5);
            individual.disabled = true;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = true;
            mostrar();
        } else if (juego.value == "Rocket League") {
            removeOptions();
            plataforma.add(playstation);
            plataforma.add(xbox);
            plataforma.add(pc);
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option2);
            modalidad.add(option3);
            individual.disabled = false;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = false;
            ocultar();
        } else if (juego.value == "Super Smash Bros") {
            removeOptions();
            plataforma.add(nintendoSwitch);
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option2);
            individual.disabled = false;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = false;
            ocultar();
        } else if (juego.value == "Valorant") {
            removeOptions();
            plataforma.add(pc);
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option2);
            modalidad.add(option5);
            individual.disabled = false;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = false;
            ocultar();
        }
    }
    function removeOptions() {
        for (i = plataforma.length - 1; i >= 0; i--) {
            plataforma.remove(i);
        }
        for (i = capacidad.length - 1; i >= 0; i--) {
            capacidad.remove(i);
        }
        for (i = modalidad.length - 1; i >= 0; i--) {
            modalidad.remove(i);
        }
    }
}
if (sessionValue == "admin") {
    if (nameUser == "playstation") {
        juego.add(fifa);
        juego.add(nba);
        juego.add(overwatch);
        juego.add(rainbow);
        juego.add(rocket);
        plataforma.add(playstation);
    } else if (nameUser == "xbox") {
        juego.add(fifa);
        juego.add(nba);
        juego.add(overwatch);
        juego.add(rainbow);
        juego.add(rocket);
        plataforma.add(xbox);
    } else if (nameUser == "epicgames") {
        juego.add(rocket);
        plataforma.add(playstation);
        plataforma.add(xbox);
        plataforma.add(pc);
    } else if (nameUser == "nintendoswitch") {
        juego.add(marioKart);
        juego.add(pokemon);
        juego.add(smash);
        plataforma.add(nintendoSwitch);
    } else if (nameUser == "steam") {
        juego.add(csgo);
        plataforma.add(pc);
    } else if (nameUser == "riotgames") {
        juego.add(lol);
        juego.add(valorant);
        plataforma.add(pc);
    } else if (nameUser == "blizzard") {
        juego.add(hearthstone);
        juego.add(overwatch);
        plataforma.add(pc);
    } else {
        juego.add(brawlStars);
        juego.add(clashRoyale);
        plataforma.add(movil);
    }
    function validacio() {
        if (juego.value == "Brawl Stars") {
            removeOptions();
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option2);
            individual.disabled = false;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = false;
            ocultar();
        } else if (juego.value == "Clash Royale") {
            removeOptions();
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option2);
            individual.disabled = false;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = false;
            ocultar();
        } else if (juego.value == "CS:GO") {
            removeOptions();
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option5);
            individual.disabled = false;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = false;
            ocultar();
        } else if (juego.value == "FIFA") {
            removeOptions();
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            individual.disabled = false;
            equipo.disabled = true;
            individual.checked = true;
            equipo.checked = false;
            ocultar();
        } else if (juego.value == "Hearthstone") {
            removeOptions();
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            individual.disabled = false;
            equipo.disabled = true;
            individual.checked = true;
            equipo.checked = false;
            ocultar();
        } else if (juego.value == "LOL") {
            removeOptions();
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option5);
            individual.disabled = true;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = true;
            mostrar();
        } else if (juego.value == "Mario Kart") {
            removeOptions();
            capacidad.add(option8);
            individual.disabled = false;
            equipo.disabled = true;
            individual.checked = true;
            equipo.checked = false;
            ocultar();
        } else if (juego.value == "NBA 2K") {
            removeOptions();
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option3);
            modalidad.add(option5);
            individual.disabled = true;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = true;
            mostrar();
        } else if (juego.value == "Overwatch") {
            removeOptions();
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option6);
            individual.disabled = true;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = true;
            mostrar();
        } else if (juego.value == "Pokemon") {
            removeOptions();
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            individual.disabled = false;
            equipo.disabled = true;
            individual.checked = true;
            equipo.checked = false;
            ocultar();
        } else if (juego.value == "Rainbow Six Siege") {
            removeOptions();
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option5);
            individual.disabled = true;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = true;
            mostrar();
        } else if (juego.value == "Rocket League") {
            removeOptions();
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option2);
            modalidad.add(option3);
            individual.disabled = false;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = false;
            ocultar();
        } else if (juego.value == "Super Smash Bros") {
            removeOptions();
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option2);
            individual.disabled = false;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = false;
            ocultar();
        } else if (juego.value == "Valorant") {
            removeOptions();
            capacidad.add(option8);
            capacidad.add(option16);
            capacidad.add(option32);
            capacidad.add(option64);
            modalidad.add(option2);
            modalidad.add(option5);
            individual.disabled = false;
            equipo.disabled = false;
            individual.checked = false;
            equipo.checked = false;
            ocultar();
        }
    }
    function removeOptions() {
        for (i = capacidad.length - 1; i >= 0; i--) {
            capacidad.remove(i);
        }
        for (i = modalidad.length - 1; i >= 0; i--) {
            modalidad.remove(i);
        }
    }
}
