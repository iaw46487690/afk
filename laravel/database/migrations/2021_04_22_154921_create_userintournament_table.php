<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateUserintournamentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usersintournaments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tournament_id')->unsigned()->index();
            $table->foreign('tournament_id')->references('id')->on('tournaments');
            $table->bigInteger('user_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('username')->nullable();
            $table->integer('match64')->nullable();
            $table->boolean('round64')->nullable();
            $table->integer('match32')->nullable();
            $table->boolean('round32')->nullable();
            $table->integer('match16')->nullable();
            $table->boolean('round16')->nullable();
            $table->integer('match8')->nullable();
            $table->boolean('round8')->nullable();
            $table->integer('match4')->nullable();
            $table->boolean('round4')->nullable();
            $table->integer('match2')->nullable();
            $table->boolean('round2')->nullable();
            /*$table->integer('match1')->nullable();
            $table->boolean('round1')->nullable();*/
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usersintournaments');
    }
}
