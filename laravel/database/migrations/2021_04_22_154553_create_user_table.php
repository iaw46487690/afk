<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique(); // No se pueden ,
            $table->string('email')->unique();
            $table->string('password');
            $table->string('type')->default('estandar');
            $table->string('img')->default('default.jpg');
            $table->string('backImg')->default('skyrim.webp');
            $table->string('twitter')->nullable();
            $table->string('youtube')->nullable();
            $table->string('twitch')->nullable();
            $table->string('instagram')->nullable();
            $table->string('idps')->nullable();
            $table->string('idxbox')->nullable();
            $table->string('idswitch')->nullable();
            $table->string('idsteam')->nullable();
            $table->string('idepicgames')->nullable();
            $table->string('idblizzard')->nullable();
            $table->string('idsupercell')->nullable();
            $table->string('idriotgames')->nullable();
            $table->integer('points')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
