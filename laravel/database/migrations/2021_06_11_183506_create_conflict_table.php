<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConflictTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conflicts', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tournament_id')->unsigned()->index();
            $table->foreign('tournament_id')->references('id')->on('tournaments');
            $table->bigInteger('user_id_1')->unsigned()->index()->nullable();
            $table->foreign('user_id_1')->references('id')->on('users');
            $table->string('player1')->nullable();
            $table->bigInteger('user_id_2')->unsigned()->index()->nullable();
            $table->foreign('user_id_2')->references('id')->on('users');
            $table->string('player2')->nullable();
            $table->string('round')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conflicts');
    }
}
