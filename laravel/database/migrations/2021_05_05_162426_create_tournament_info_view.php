<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateTournamentInfoView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW tournament_infos
            AS
            SELECT
                tournaments.id,
                tournaments.name,
                tournaments.game_id,
                games.name AS game,
                tournaments.user_id,
                tournaments.platform,
                COUNT(DISTINCT usersintournaments.user_id) AS players,
                tournaments.capacity,
                tournaments.teamCapacity,
                tournaments.official,
                tournaments.date,
                games.img
            FROM
                tournaments
                LEFT JOIN games ON tournaments.game_id = games.id
                LEFT JOIN usersintournaments ON tournaments.id = usersintournaments.tournament_id
            GROUP BY
                tournaments.id;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS tournament_infos;");
    }
}
