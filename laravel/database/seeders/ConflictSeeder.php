<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConflictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('conflicts')->insert([
            'tournament_id' => 13,
            'user_id_1' => 1,
            'player1' => 'Alejandro',
            'user_id_2' => 5,
            'player2' => 'Jorge',
            'round' => 'Semifinales'
        ]);

        DB::table('conflicts')->insert([
            'tournament_id' => 13,
            'user_id_1' => 2,
            'player1' => 'Pol',
            'user_id_2' => 4,
            'player2' => 'Alba',
            'round' => 'Semifinales'
        ]);
    }
}
