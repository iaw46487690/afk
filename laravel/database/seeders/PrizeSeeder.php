<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PrizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prizes')->insert([
            'name' => 'PS5',
            'points' => 500000,
            'img' => 'ps5.jpg'
        ]);

        DB::table('prizes')->insert([
            'name' => 'Silla Gaming',
            'points' => 200000,
            'img' => 'sillaGaming.webp'
        ]);

        DB::table('prizes')->insert([
            'name' => '50€ PSN',
            'points' => 50000,
            'img' => 'psn50.jpg'
        ]);

        DB::table('prizes')->insert([
            'name' => 'Mando PS5',
            'points' => 55000,
            'img' => 'ps5dualshock.jpg'
        ]);

        DB::table('prizes')->insert([
            'name' => 'PSPlus 1 año',
            'points' => 50000,
            'img' => 'psplus12.jpg'
        ]);

        DB::table('prizes')->insert([
            'name' => 'Raton + Teclado Gaming',
            'points' => 35000,
            'img' => 'setGaming.jpg'
        ]);
    }
}
