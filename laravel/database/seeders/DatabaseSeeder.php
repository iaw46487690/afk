<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(UserSeeder::class);
        $this->call(GameSeeder::class);
        $this->call(TournamentSeeder::class);
        $this->call(PrizeSeeder::class);
        $this->call(UserInTournamentSeeder::class);
        $this->call(ConflictSeeder::class);
    }
}
