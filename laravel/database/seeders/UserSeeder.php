<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Alejandro',
            'email' => 'alejandro@gmail.com',
            'password' => 'contraseña',
            'idps' => 'AlejandroCR4',
            'idepicgames' => 'AlejandroCR4',
            'idblizzard' => 'Carreño',
            'points' => 50000
        ]);

        DB::table('users')->insert([
            'name' => 'Pol',
            'email' => 'pol@gmail.com',
            'password' => 'contraseña',
            'twitter' => 'https://twitter.com/Pol_P11',
            'idps' => 'Pol_P11',
            'idepicgames' => 'Pol_P11',
            'idblizzard' => 'Pol_P11',
            'points' => 50000
        ]);

        DB::table('users')->insert([
            'name' => 'Dani',
            'email' => 'dani@gmail.com',
            'password' => 'contraseña'
        ]);

        DB::table('users')->insert([
            'name' => 'Alba',
            'email' => 'alba@gmail.com',
            'password' => 'contraseña',
            'points' => 500000
        ]);

        DB::table('users')->insert([
            'name' => 'Jorge',
            'email' => 'jorge@gmail.com',
            'password' => 'contraseña'
        ]);

        DB::table('users')->insert([
            'name' => 'Luis',
            'email' => 'luis@gmail.com',
            'password' => 'contraseña'
        ]);

        for ($i=1; $i <= 60; $i++) {
            DB::table('users')->insert([
                'name' => 'User'.$i,
                'email' => 'user'.$i.'@gmail.com',
                'password' => 'contraseña'
            ]);
        }

        $array = array("playstation", "xbox", "epicgames", "nintendoswitch", "steam", "riotgames", "blizzard", "supercell");
        for ($i=0; $i < count($array); $i++) {
            DB::table('users')->insert([
                'name' => $array[$i],
                'email' => $array[$i].'@gmail.com',
                'password' => 'contraseña',
                'type' => 'admin'
            ]);
        }
    }
}
