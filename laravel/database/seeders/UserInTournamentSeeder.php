<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserInTournamentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bool = true;
        $bool32 = false;
        $bool16 = false;
        $bool8 = false;
        $bool4 = false;
        $bool2 = false;
        $bool1 = false;
        $m32 = 0;
        $m16 = 0;
        $m8 = 0;
        $m4 = 0;
        $m2 = 0;
        $m32b = 0;
        $m16b = 0;
        $m8b = 0;
        $m4b = 0;
        $m2b = 0;
        for ($i=1; $i <= 64; $i++) {
            if ($bool) {
                $bool = false;
            } else {
                $bool = true;
            }
            if ($i % 2 == 0 && $bool) {
                $bool32 = true;
                $m32++;
                $m32b = $m32;
            } else {
                $bool32 = false;
                $m32b = null;
            }
            if ($i % 4 == 0 && $bool32) {
                $bool16 = true;
                $m16++;
                $m16b = $m16;
            } else {
                $bool16 = false;
                $m16b = null;
            }
            if ($i % 8 == 0 && $bool16) {
                $bool8 = true;
                $m8++;
                $m8b = $m8;
            } else {
                $bool8 = false;
                $m8b = null;
            }
            if ($i % 16 == 0 && $bool8) {
                $bool4 = true;
                $m4++;
                $m4b = $m4;
            } else {
                $bool4 = false;
                $m4b = null;
            }
            if ($i % 32 == 0 && $bool4) {
                $bool2 = true;
                $m2++;
                $m2b = $m2;
            } else {
                $bool2 = false;
                $m2b = null;
            }
            if ($i == 64) {
                $bool1 = true;
            }

            $name = DB::table('users')->select('name')->where('id', '=', $i)->first();
            DB::table('usersintournaments')->insert([
                'user_id' => $i,
                'username' => $name->name,
                'tournament_id' => 1,
                'round64' => $bool,
                'match64' => $i,
                'round32' => $bool16,
                'match32' => $m32b,
                'round16' => $bool8,
                'match16' => $m16b,
                'round8' => $bool4,
                'match8' => $m8b,
                'round4' => $bool2,
                'match4' => $m4b,
                'round2' => $bool1,
                'match2' => $m2b
            ]);
        }

        for ($i=1; $i < 8; $i++) {
            $name = DB::table('users')->select('name')->where('id', '=', $i)->first();
            DB::table('usersintournaments')->insert([
                'user_id' => $i,
                'username' => $name->name,
                'tournament_id' => 3
            ]);
        }

        DB::table('usersintournaments')->insert([
            'user_id' => 1,
            'username' => 'Alejandro',
            'tournament_id' => 13,
            'round8' => true,
            'match8' => 2,
            'round4' => true,
            'match4' => 1
        ]);

        DB::table('usersintournaments')->insert([
            'user_id' => 2,
            'username' => 'Pol',
            'tournament_id' => 13,
            'round8' => true,
            'match8' => 8,
            'round4' => true,
            'match4' => 4
        ]);

        DB::table('usersintournaments')->insert([
            'user_id' => 3,
            'username' => 'Dani',
            'tournament_id' => 13,
            'round8' => false,
            'match8' => 7
        ]);

        DB::table('usersintournaments')->insert([
            'user_id' => 4,
            'username' => 'Alba',
            'tournament_id' => 13,
            'round8' => true,
            'match8' => 5,
            'round4' => true,
            'match4' => 3
        ]);

        DB::table('usersintournaments')->insert([
            'user_id' => 5,
            'username' => 'Jorge',
            'tournament_id' => 13,
            'round8' => true,
            'match8' => 3,
            'round4' => true,
            'match4' => 2
        ]);

        DB::table('usersintournaments')->insert([
            'user_id' => 6,
            'username' => 'Luis',
            'tournament_id' => 13,
            'round8' => false,
            'match8' => 4
        ]);

        DB::table('usersintournaments')->insert([
            'user_id' => 7,
            'username' => 'User1',
            'tournament_id' => 13,
            'round8' => false,
            'match8' => 6
        ]);

        DB::table('usersintournaments')->insert([
            'user_id' => 8,
            'username' => 'User2',
            'tournament_id' => 13,
            'round8' => false,
            'match8' => 1
        ]);
    }
}
