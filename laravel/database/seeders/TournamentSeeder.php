<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class TournamentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tournaments')->insert([
            'name' => 'Torneo Brawl Stars',
            'game_id' => 1,
            'user_id' => 74,
            'platform' => 'Movil',
            'capacity' => '64',
            'teamCapacity' => '1',
            'official' => true,
            'date' => Carbon::parse('2021-05-11 23:00')->format('Y-m-d H:i')
        ]);

        DB::table('tournaments')->insert([
            'name' => 'Torneo CS:GO',
            'game_id' => 3,
            'user_id' => 71,
            'platform' => 'PC',
            'capacity' => '16',
            'teamCapacity' => '5',
            'official' => true,
            'date' => Carbon::parse('2021-06-11 18:44')->format('Y-m-d H:i')
        ]);

        DB::table('tournaments')->insert([
            'name' => 'Torneo FIFA',
            'game_id' => 4,
            'user_id' => 67,
            'platform' => 'PlayStation',
            'capacity' => '8',
            'teamCapacity' => '1',
            'official' => true,
            'date' => Carbon::parse('2021-06-12 18:44')->format('Y-m-d H:i')
        ]);

        DB::table('tournaments')->insert([
            'name' => 'Torneo FIFA',
            'game_id' => 4,
            'user_id' => 1,
            'platform' => 'PlayStation',
            'capacity' => '64',
            'teamCapacity' => '1',
            'official' => false,
            'date' => Carbon::parse('2021-06-13 17:00')->format('Y-m-d H:i')
        ]);

        DB::table('tournaments')->insert([
            'name' => 'Torneo FIFA',
            'game_id' => 4,
            'user_id' => 2,
            'platform' => 'XBOX',
            'capacity' => '64',
            'teamCapacity' => '1',
            'official' => false,
            'date' => Carbon::parse('2021-06-14 17:00')->format('Y-m-d H:i')
        ]);

        DB::table('tournaments')->insert([
            'name' => 'Torneo FIFA',
            'game_id' => 4,
            'user_id' => 68,
            'platform' => 'XBOX',
            'capacity' => '64',
            'teamCapacity' => '1',
            'official' => true,
            'date' => Carbon::parse('2021-06-15 17:00')->format('Y-m-d H:i')
        ]);

        DB::table('tournaments')->insert([
            'name' => 'Torneo FIFA',
            'game_id' => 4,
            'user_id' => 68,
            'platform' => 'XBOX',
            'capacity' => '64',
            'teamCapacity' => '1',
            'official' => true,
            'date' => Carbon::parse('2021-06-16 17:00')->format('Y-m-d H:i')
        ]);

        DB::table('tournaments')->insert([
            'name' => 'Torneo Rocket League',
            'game_id' => 12,
            'user_id' => 69,
            'platform' => 'PC',
            'capacity' => '8',
            'teamCapacity' => '3',
            'official' => true,
            'date' => Carbon::parse('2021-06-17 18:44')->format('Y-m-d H:i')
        ]);

        DB::table('tournaments')->insert([
            'name' => 'Rocket League',
            'game_id' => 12,
            'user_id' => 3,
            'platform' => 'PlayStation',
            'capacity' => '32',
            'teamCapacity' => '2',
            'official' => false,
            'date' => Carbon::parse('2021-06-18 17:00')->format('Y-m-d H:i')
        ]);

        DB::table('tournaments')->insert([
            'name' => 'Torneo Pokemon',
            'game_id' => 10,
            'user_id' => 4,
            'platform' => 'Nintendo switch',
            'capacity' => '64',
            'teamCapacity' => '1',
            'official' => false,
            'date' => Carbon::parse('2021-06-19 17:00')->format('Y-m-d H:i')
        ]);

        DB::table('tournaments')->insert([
            'name' => 'Torneo Pokemon',
            'game_id' => 10,
            'user_id' => 70,
            'platform' => 'Nintendo switch',
            'capacity' => '32',
            'teamCapacity' => '1',
            'official' => true,
            'date' => Carbon::parse('2021-06-20 16:00')->format('Y-m-d H:i')
        ]);

        DB::table('tournaments')->insert([
            'name' => 'Torneo Pokemon',
            'game_id' => 10,
            'user_id' => 70,
            'platform' => 'Nintendo switch',
            'capacity' => '8',
            'teamCapacity' => '1',
            'official' => true,
            'date' => Carbon::parse('2021-06-10 19:00')->format('Y-m-d H:i')
        ]);

        DB::table('tournaments')->insert([
            'name' => 'Torneo FIFA',
            'game_id' => 4,
            'user_id' => 68,
            'platform' => 'XBOX',
            'capacity' => '8',
            'teamCapacity' => '1',
            'official' => true,
            'date' => Carbon::parse('2021-06-10 17:00')->format('Y-m-d H:i')
        ]);

        DB::table('tournaments')->insert([
            'name' => 'Torneo FIFA',
            'game_id' => 4,
            'user_id' => 5,
            'platform' => 'PlayStation',
            'capacity' => '8',
            'teamCapacity' => '1',
            'official' => false,
            'date' => Carbon::parse('2021-06-10 17:00')->format('Y-m-d H:i')
        ]);
    }
}
