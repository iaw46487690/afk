<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('games')->insert([
            'name' => 'Brawl Stars',
            'platform' => 'Movil',
            'capacity' => '8-16-32-64',
            'teamCapacity' => '1-2',
            'img' => 'brawlStars.png',
            'video' => 'CaryjOdYFa0'
        ]);

        DB::table('games')->insert([
            'name' => 'Clash Royale',
            'platform' => 'Movil',
            'capacity' => '8-16-32-64',
            'teamCapacity' => '1-2',
            'img' => 'clashRoyale.jpg',
            'video' => '1RC1yxqTTd8'
        ]);

        DB::table('games')->insert([
            'name' => 'CS:GO',
            'platform' => 'PC',
            'capacity' => '8-16-32-64',
            'teamCapacity' => '1-5',
            'img' => 'csgo.jpg',
            'video' => 'edYCtaNueQY'
        ]);

        DB::table('games')->insert([
            'name' => 'FIFA',
            'platform' => 'Consolas',
            'capacity' => '8-16-32-64',
            'teamCapacity' => '1',
            'img' => 'fifa.jpg',
            'video' => 'tuLAn9adQpI'
        ]);

        DB::table('games')->insert([
            'name' => 'Hearthstone',
            'platform' => 'PC',
            'capacity' => '8-16-32-64',
            'teamCapacity' => '1',
            'img' => 'hearthstone.png',
            'video' => 'i_DvY_Vbn6I'
        ]);

        DB::table('games')->insert([
            'name' => 'LOL',
            'platform' => 'PC',
            'capacity' => '8-16-32-64',
            'teamCapacity' => '5',
            'img' => 'lol.webp',
            'video' => '8PbhGt8XxSM'
        ]);

        DB::table('games')->insert([
            'name' => 'Mario Kart',
            'platform' => 'Switch',
            'capacity' => '8',
            'teamCapacity' => '1',
            'img' => 'marioKart.jpg',
            'video' => 'tKlRN2YpxRE'
        ]);

        DB::table('games')->insert([
            'name' => 'NBA 2K',
            'platform' => 'Consolas',
            'capacity' => '8-16-32-64',
            'teamCapacity' => '3-5',
            'img' => 'nba.webp',
            'video' => 'LUdPHjOaKrk'
        ]);

        DB::table('games')->insert([
            'name' => 'Overwatch',
            'platform' => 'Consolas y PC',
            'capacity' => '8-16-32-64',
            'teamCapacity' => '6',
            'img' => 'overwatch.webp',
            'video' => 'dushZybUYnM'
        ]);

        DB::table('games')->insert([
            'name' => 'Pokemon',
            'platform' => 'Switch',
            'capacity' => '8-16-32-64',
            'teamCapacity' => '1',
            'img' => 'pokemon.jpg',
            'video' => 'uBYORdr_TY8'
        ]);

        DB::table('games')->insert([
            'name' => 'Rainbow Six Siege',
            'platform' => 'Consolas y PC',
            'capacity' => '8-16-32-64',
            'teamCapacity' => '5',
            'img' => 'rainbowSix.jpg',
            'video' => 'kqDwcjF5gW8'
        ]);

        DB::table('games')->insert([
            'name' => 'Rocket League',
            'platform' => 'Consolas y PC',
            'capacity' => '8-16-32-64',
            'teamCapacity' => '1-2-3',
            'img' => 'rocketLeague.webp',
            'video' => 'zjPBq1KR3aA'
        ]);

        DB::table('games')->insert([
            'name' => 'Super Smash Bros',
            'platform' => 'Switch',
            'capacity' => '8-16-32-64',
            'teamCapacity' => '1-2',
            'img' => 'superSmash.jpg',
            'video' => 'TUzZKLirkrE'
        ]);

        DB::table('games')->insert([
            'name' => 'Valorant',
            'platform' => 'PC',
            'capacity' => '8-16-32-64',
            'teamCapacity' => '1-2-5',
            'img' => 'valorant.webp',
            'video' => 'e_E9W2vsRbQ'
        ]);
    }
}
