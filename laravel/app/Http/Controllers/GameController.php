<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\FilterRequest;

use App\Models\TournamentInfo;
use App\Models\Game;

class GameController extends Controller
{
    public $tournament;

    public function __construct() {
        $this->tournament = new TournamentInfo;
        $this->game = new Game;
        $this->gameFilters = $this->game->gameFilters();
    }

    /**
     * Muestra todos los torneos de 1 juego especifico
     */
    public function showGamePage($game) {
        $tournaments = $this->tournament->query();
        $tournaments->date();
        $tournaments->gameFilter($game);
        $tournaments = $tournaments->get();

        $gameInfo = $this->game->gameInfo($game)->first();

        return view('juegos')
        ->with('tournaments', $tournaments)
        ->with('gameFilters', $this->gameFilters)
        ->with('gameInfo', $gameInfo)
        ->with('game', $game);
    }

    /**
     * Muestra todos los torneos aplicando filtros
     */
    public function filterGames(FilterRequest $request) {
        $search = '';
        $game = '';
        $gameInfo = '';
        $request->flash();

        $tournaments = $this->tournament->query();
        $tournaments->date();

        if ($request->filled('game_id')) {
            $game = $request->input('game_id');
            $tournaments->gameFilter($game);
            $gameInfo = $this->game->gameInfo($game)->first();
        }

        if ($request->filled('search')) {
            $search = $request->input('search');
            $tournaments->search($search);
        }

        // Filtro oficial
        if ($request->isNotFilled('official') || $request->isNotFilled('noOfficial')) {
            if ($request->filled('noOfficial')) {
                $official = false;
                $tournaments->official($official);
            } else if ($request->filled('official')) {
                $official = true;
                $tournaments->official($official);
            }
        }
        // Filtro plataformas
        if ($request->filled('PlayStation') || $request->filled('XBOX') ||
            $request->filled('PC') || $request->filled('NintendoSwitch') ||
            $request->filled('Movil')) {
            $platforms = [];
            if ($request->filled('PlayStation')) {
                array_push($platforms, "PlayStation");
            }
            if ($request->filled('XBOX')) {
                array_push($platforms, "XBOX");
            }
            if ($request->filled('PC')) {
                array_push($platforms, "PC");
            }
            if ($request->filled('NintendoSwitch')) {
                array_push($platforms, "Nintendo switch");
            }
            if ($request->filled('Movil')) {
                array_push($platforms, "Movil");
            }
            if ($platforms[0] != "Movil") {
                array_push($platforms, "Todas las plataformas");
            }
            $tournaments->platformFilterArray($platforms);
        }

        //$tournaments = $tournaments->get();

        return view('juegos')->with('tournaments', $tournaments->get())
        ->with('gameFilters', $this->gameFilters)
        ->with('search', $search)
        ->with('gameInfo', $gameInfo)
        ->with('game', $game);
    }
}
