<?php

namespace App\Http\Controllers;
use App\Models\Conflict;
use App\Models\Game;
use App\Models\User;
use App\Models\Tournament;
use App\Models\TournamentInfo;
use App\Models\UsersInTournaments;
use Illuminate\Support\Facades\DB;

use App\Http\Requests\CreateTournamentRequest;

class TournamentController extends Controller
{
    public $tournament;

    public function __construct() {
        $this->tournament = new TournamentInfo;
        $this->conflict = new Conflict;
        $this->userTournament = new UsersInTournaments;
        $this->game = new Game;
        $this->gameFilters = $this->game->gameFilters();
    }

    public function showCreateTournament() {
        return view('crearTorneo')
        ->with('gameFilters', $this->gameFilters);
    }

    public function createNewTournament(CreateTournamentRequest $createTournamentRequest) {
        $userInfo = app('request')->session()->get('Usuario');
        $name = $userInfo['name'];
        $info = User::where('name', '=', $name)->first();
        $newTournament = new Tournament;
        $newTournament->name = $createTournamentRequest->input('name');
        $game = $createTournamentRequest->input('juego');
        $id = $this->game->findIdGame($game)->value('id');
        $newTournament->game_id = $id;
        $newTournament->user_id = $info->id;
        $newTournament->platform = $createTournamentRequest->input('plataforma');
        $newTournament->capacity = $createTournamentRequest->input('capacidad');
        if ($createTournamentRequest->input('equipo') == "individual") {
            $newTournament->teamCapacity = 1;
        } else {
            $newTournament->teamCapacity = $createTournamentRequest->input('modalidad');
        }
        if ($createTournamentRequest->session()->get('Usuario')['type'] == 'admin') {
            $newTournament->official = 1;
        } else {
            $newTournament->official = 0;
        }
        $date = $createTournamentRequest->input('date');
        $hour = $createTournamentRequest->input('hour');
        $dateFormated = $date . " " . $hour;
        $newTournament->date = $dateFormated;
        $newTournament->save();

        if ($userInfo['type'] == 'estandar') {
            $this->registerTournament($newTournament->id);
        }

        return view('confirmarTorneo')
        ->with('gameFilters', $this->gameFilters);
    }

    public function registerTournament($tournamentId) {
        $userInfo = app('request')->session()->get('Usuario');
        $name = $userInfo['name'];
        $info = User::where('name', '=', $name)->first();
        $userId = $info->id;
        $username = $info->name;

        $newPlayer = new UsersInTournaments;
        $newPlayer->tournament_id = $tournamentId;
        $newPlayer->user_id = $userId;
        $newPlayer->username = $username;
        $newPlayer->save();

        return redirect()->back();
    }

    public function removeFromTournament($tournamentId) {
        $userInfo = app('request')->session()->get('Usuario');
        $name = $userInfo['name'];
        $info = User::where('name', '=', $name)->first();
        $userId = $info->id;

        $this->userTournament->playing($userId, $tournamentId)->delete();

        return redirect()->back();
    }

    public function showTournament($tournament) {
        $userInfo = app('request')->session()->get('Usuario');
        $name = $userInfo['name'];
        $info = User::where('name', '=', $name)->first();
        $userId = $info->id;

        $expiredOrPlaying = 'nothing';
        if ($userInfo['type'] == 'estandar') {
            $playing = $this->userTournament->playing($userId, $tournament)->first();
            $expired = $this->userTournament->expired($userId, $tournament)->first();
            if ($expired) {
                $expiredOrPlaying = 'expired';
            } else if ($playing) {
                $expiredOrPlaying = 'playing';
            }
        } else {
            $expired = $this->userTournament->expiredAdmin($userId, $tournament)->first();
            if ($expired) {
                $expiredOrPlaying = 'expired';
            }
        }
        $tournamentInfo = $this->tournament->getTournamentById($tournament)->first();



        // Array Matches
        if ($expiredOrPlaying == 'expired') {
            $matches = array();
            if ($tournamentInfo->capacity >= 64) {
                $matches64 = $this->userTournament->tournament($tournament)->orderByMatch64()->get();
                array_push($matches, $matches64);
            } elseif ($tournamentInfo->capacity == 64) {
                $matches64 = $this->userTournament->tournament($tournament)->orderByFirstMatch64()->get();
                array_push($matches, $matches64);
            }
            if ($tournamentInfo->capacity >= 32) {
                $matches32 = $this->userTournament->tournament($tournament)->orderByMatch32()->get();
                array_push($matches, $matches32);
            } elseif ($tournamentInfo->capacity == 32) {
                $matches32 = $this->userTournament->tournament($tournament)->orderByFirstMatch32()->get();
                array_push($matches, $matches32);
            }
            if ($tournamentInfo->capacity > 16) {
                $matches16 = $this->userTournament->tournament($tournament)->orderByMatch16()->get();
                array_push($matches, $matches16);
            } elseif ($tournamentInfo->capacity == 16) {
                $matches16 = $this->userTournament->tournament($tournament)->orderByFirstMatch16()->get();
                array_push($matches, $matches16);
            }
            if ($tournamentInfo->capacity > 8) {
                $matches8 = $this->userTournament->tournament($tournament)->orderByMatch8()->get();
                array_push($matches, $matches8);
            } elseif ($tournamentInfo->capacity == 8) {
                $matches8 = $this->userTournament->tournament($tournament)->orderByFirstMatch8()->get();
                array_push($matches, $matches8);
            }
            $matches4 = $this->userTournament->tournament($tournament)->orderByMatch4()->get();
            $matches2 = $this->userTournament->tournament($tournament)->orderByMatch2()->get();
            array_push($matches, $matches4, $matches2);
        }


        return view('torneo')
        ->with('tournament', $tournamentInfo)
        ->with('userInfo', $info)
        ->with('matches', $matches ?? '')
        ->with('gameFilters', $this->gameFilters)
        ->with('expiredOrPlaying', $expiredOrPlaying);
    }

    public function looser($id) {
        $userInfo = app('request')->session()->get('Usuario');
        $name = $userInfo['name'];
        $info = User::where('name', '=', $name)->first();
        $infoUiT = UsersInTournaments::where('tournament_id', '=', $id)->where('user_id', '=', $info->id)->first();
        $tournament = Tournament::where('id', '=', $infoUiT->tournament_id)->first();
        $tournamentId = $infoUiT->tournament_id;
        $numPlayers = $tournament->capacity;
        $official = $tournament->official;
        $points = 0;
        if ($official) {
            if ($numPlayers == 64) {
                $points = 6400;
            } else if ($numPlayers == 32) {
                $points = 3200;
            } else if ($numPlayers == 16) {
                $points = 1600;
            } else if ($numPlayers == 8) {
                $points = 800;
            }
        }

        $newConflict = new Conflict;
        $newConflict->tournament_id = $tournamentId;
        $newConflict->user_id_1 = $info->id;
        $newConflict->player1 = $info->name;

        if (!is_null($infoUiT->match2)) {
            $infoUiT->round2 = false;
            $match = $infoUiT->match2;
            if ($match % 2 != 0) {
                $match += 1;
                $aux = $match;
            } else {
                $aux = $match - 1;
            }
            $infoUiT2 = UsersInTournaments::where('tournament_id', '=', $tournamentId)->where('match2', '=', $aux)->first();
            $info2 = User::where('id', '=', $infoUiT2->user_id)->first();
            if (!is_null($infoUiT2->round2) && $infoUiT2->round2 == false) {
                $info2->points -= $points / 2;
                $newConflict->user_id_2 = $info2->id;
                $newConflict->player2 = $info2->name;
                $newConflict->round = 'Final';
                $newConflict->save();
            } else {
                $info->points += $points / 2;
            }
        } else if (!is_null($infoUiT->match4)) {
            $infoUiT->round4 = false;
            $match = $infoUiT->match4;
            if ($match % 2 != 0) {
                $match += 1;
                $aux = $match;
            } else {
                $aux = $match - 1;
            }
            $infoUiT2 = UsersInTournaments::where('tournament_id', '=', $tournamentId)->where('match4', '=', $aux)->first();
            $info2 = User::where('id', '=', $infoUiT2->user_id)->first();
            if (!is_null($infoUiT2->round4) && $infoUiT->round4 == $infoUiT2->round4) {
                $info2->points -= $points / 4;
                $newConflict->user_id_2 = $info2->id;
                $newConflict->player2 = $info2->name;
                $newConflict->round = 'Final';
                $newConflict->save();
            } else {
                $info->points += $points / 4;
            }
        } else if (!is_null($infoUiT->match8)) {
            $infoUiT->round8 = false;
            $match = $infoUiT->match8;
            if ($match % 2 != 0) {
                $match += 1;
                $aux = $match;
            } else {
                $aux = $match - 1;
            }
            $infoUiT2 = UsersInTournaments::where('tournament_id', '=', $tournamentId)->where('match8', '=', $aux)->first();
            $info2 = User::where('id', '=', $infoUiT2->user_id)->first();
            if (!is_null($infoUiT2->round8) && $infoUiT->round8 == $infoUiT2->round8) {
                $info2->points -= $points / 8;
                $newConflict->user_id_2 = $info2->id;
                $newConflict->player2 = $info2->name;
                $newConflict->round = 'Final';
                $newConflict->save();
            } else {
                $info->points += $points / 8;
            }
        } else if (!is_null($infoUiT->match16)) {
            $infoUiT->round16 = false;
            $match = $infoUiT->match16;
            if ($match % 2 != 0) {
                $match += 1;
                $aux = $match;
            } else {
                $aux = $match - 1;
            }
            $infoUiT2 = UsersInTournaments::where('tournament_id', '=', $tournamentId)->where('match16', '=', $aux)->first();
            $info2 = User::where('id', '=', $infoUiT2->user_id)->first();
            if (!is_null($infoUiT2->round16) && $infoUiT->round16 == $infoUiT2->round16) {
                $info2->points -= $points / 16;
                $newConflict->user_id_2 = $info2->id;
                $newConflict->player2 = $info2->name;
                $newConflict->round = 'Final';
                $newConflict->save();
            } else {
                $info->points += $points / 16;
            }
        } else if (!is_null($infoUiT->match32)) {
            $infoUiT->round32 = false;
            $match = $infoUiT->match32;
            if ($match % 2 != 0) {
                $match += 1;
                $aux = $match;
            } else {
                $aux = $match - 1;
            }
            $infoUiT2 = UsersInTournaments::where('tournament_id', '=', $tournamentId)->where('match32', '=', $aux)->first();
            $info2 = User::where('id', '=', $infoUiT2->user_id)->first();
            if (!is_null($infoUiT2->round32) && $infoUiT->round32 == $infoUiT2->round32) {
                $info2->points -= $points / 32;
                $newConflict->user_id_2 = $info2->id;
                $newConflict->player2 = $info2->name;
                $newConflict->round = 'Final';
                $newConflict->save();
            } else {
                $info->points += $points / 32;
            }
        } else if (!is_null($infoUiT->match64)) {
            $infoUiT->round64 = false;
            $match = $infoUiT->match64;
            if ($match % 2 != 0) {
                $match += 1;
                $aux = $match;
            } else {
                $aux = $match - 1;
            }
            $infoUiT2 = UsersInTournaments::where('tournament_id', '=', $tournamentId)->where('match64', '=', $aux)->first();
            $info2 = User::where('id', '=', $infoUiT2->user_id)->first();
            if (!is_null($infoUiT2->round64) && $infoUiT->round64 == $infoUiT2->round64) {
                $info2->points -= $points / 64;
                $newConflict->user_id_2 = $info2->id;
                $newConflict->player2 = $info2->name;
                $newConflict->round = 'Final';
                $newConflict->save();
            } else {
                $info->points += $points / 64;
            }
        }
        $infoUiT->save();
        $info->save();
        $infoUiT2->save();
        $info2->save();

        return redirect()->back();
    }

    public function winner($id) {
        $userInfo = app('request')->session()->get('Usuario');
        $name = $userInfo['name'];
        $info = User::where('name', '=', $name)->first();
        $infoUiT = UsersInTournaments::where('tournament_id', '=', $id)->where('user_id', '=', $info->id)->first();
        $tournament = Tournament::where('id', '=', $infoUiT->tournament_id)->first();
        $tournamentId = $infoUiT->tournament_id;
        $numPlayers = $tournament->capacity;
        $official = $tournament->official;
        $points = 0;
        if ($official) {
            if ($numPlayers == 64) {
                $points = 6400;
            } else if ($numPlayers == 32) {
                $points = 3200;
            } else if ($numPlayers == 16) {
                $points = 1600;
            } else if ($numPlayers == 8) {
                $points = 800;
            }
        }

        $newConflict = new Conflict;
        $newConflict->tournament_id = $tournamentId;
        $newConflict->user_id_1 = $info->id;
        $newConflict->player1 = $info->name;

        if (!is_null($infoUiT->match2)) {
            $match = $infoUiT->match2;
            if ($match % 2 != 0) {
                $match += 1;
                $aux = $match;
            } else {
                $aux = $match - 1;
            }
            $infoUiT2 = UsersInTournaments::where('tournament_id', '=', $tournamentId)->where('match2', '=', $aux)->first();
            $info2 = User::where('id', '=', $infoUiT2->user_id)->first();
            $infoUiT->round2 = true;
            if ($infoUiT->round2 == $infoUiT2->round2) {
                $info2->points -= $points;
                $newConflict->user_id_2 = $info2->id;
                $newConflict->player2 = $info2->name;
                $newConflict->round = 'Final';
                $newConflict->save();
            } else {
                $info->points += $points;
            }
        } else if (!is_null($infoUiT->match4)) {
            $match = $infoUiT->match4;
            if ($match % 2 != 0) {
                $match += 1;
                $aux = $match;
            } else {
                $aux = $match - 1;
            }
            $infoUiT2 = UsersInTournaments::where('tournament_id', '=', $tournamentId)->where('match4', '=', $aux)->first();
            $info2 = User::where('id', '=', $infoUiT2->user_id)->first();
            $match /= 2;
            $infoUiT->match2 = $match;
            $infoUiT->round4 = true;
            if ($infoUiT->round4 == $infoUiT2->round4) {
                $newConflict->user_id_2 = $info2->id;
                $newConflict->player2 = $info2->name;
                $newConflict->round = 'Semifinales';
                $newConflict->save();
            }
        } else if (!is_null($infoUiT->match8)) {
            $match = $infoUiT->match8;
            if ($match % 2 != 0) {
                $match += 1;
                $aux = $match;
            } else {
                $aux = $match - 1;
            }
            $infoUiT2 = UsersInTournaments::where('tournament_id', '=', $tournamentId)->where('match8', '=', $aux)->first();
            $info2 = User::where('id', '=', $infoUiT2->user_id)->first();
            $match /= 2;
            $infoUiT->match4 = $match;
            $infoUiT->round8 = true;
            if ($infoUiT->round8 == $infoUiT2->round8) {
                $newConflict->user_id_2 = $info2->id;
                $newConflict->player2 = $info2->name;
                $newConflict->round = 'Cuartos';
                $newConflict->save();
            }
        } else if (!is_null($infoUiT->match16)) {
            $match = $infoUiT->match16;
            if ($match % 2 != 0) {
                $match += 1;
                $aux = $match;
            } else {
                $aux = $match - 1;
            }
            $infoUiT2 = UsersInTournaments::where('tournament_id', '=', $tournamentId)->where('match16', '=', $aux)->first();
            $info2 = User::where('id', '=', $infoUiT2->user_id)->first();
            $match /= 2;
            $infoUiT->match8 = $match;
            $infoUiT->round16 = true;
            if ($infoUiT->round16 == $infoUiT2->round16) {
                $newConflict->user_id_2 = $info2->id;
                $newConflict->player2 = $info2->name;
                $newConflict->round = 'Octavos';
                $newConflict->save();
            }
        } else if (!is_null($infoUiT->match32)) {
            $match = $infoUiT->match32;
            if ($match % 2 != 0) {
                $match += 1;
                $aux = $match;
            } else {
                $aux = $match - 1;
            }
            $infoUiT2 = UsersInTournaments::where('tournament_id', '=', $tournamentId)->where('match32', '=', $aux)->first();
            $info2 = User::where('id', '=', $infoUiT2->user_id)->first();
            $match /= 2;
            $infoUiT->match16 = $match;
            $infoUiT->round32 = true;
            if ($infoUiT->round32 == $infoUiT2->round32) {
                $newConflict->user_id_2 = $info2->id;
                $newConflict->player2 = $info2->name;
                $newConflict->round = 'Ronda2';
                $newConflict->save();
            }
        } else if (!is_null($infoUiT->match64)) {
            $match = $infoUiT->match64;
            if ($match % 2 != 0) {
                $match += 1;
                $aux = $match;
            } else {
                $aux = $match - 1;
            }
            $infoUiT2 = UsersInTournaments::where('tournament_id', '=', $tournamentId)->where('match64', '=', $aux)->first();
            $info2 = User::where('id', '=', $infoUiT2->user_id)->first();
            $match /= 2;
            $infoUiT->match32 = $match;
            $infoUiT->round64 = true;
            if ($infoUiT->round64 == $infoUiT2->round64) {
                $newConflict->user_id_2 = $info2->id;
                $newConflict->player2 = $info2->name;
                $newConflict->round = 'Ronda1';
                $newConflict->save();
            }
        }
        $infoUiT->save();
        $info->save();
        $infoUiT2->save();
        $info2->save();
        return redirect()->back();
    }

    public function showConflicts($tournament) {
        $userInfo = app('request')->session()->get('Usuario');
        $name = $userInfo['name'];
        $info = User::where('name', '=', $name)->first();

        $tournamentInfo = $this->tournament->getTournamentById($tournament)->first();

        $conflicts = $this->conflict->conflicts($tournament)->get();
        if (count($conflicts) == 0) {
            return redirect('/misTorneos');
        } else {
            return view('conflicto')
            ->with('tournament', $tournamentInfo)
            ->with('conflicts', $conflicts)
            ->with('gameFilters', $this->gameFilters);
        }
    }

    public function winnerConflict($id) {
        $position = stripos($id, '&');
        $tournamentId = substr($id, 0, $position);
        $id = substr($id, $position + 1);
        $position = stripos($id, '&');
        $userId = substr($id, 0, $position);
        $round = substr($id, $position + 1);

        $info = User::where('id', '=', $userId)->first();
        $infoUiT = UsersInTournaments::where('tournament_id', '=', $tournamentId)->where('user_id', '=', $userId)->first();
        $tournament = Tournament::where('id', '=', $tournamentId)->first();
        $numPlayers = $tournament->capacity;
        $official = $tournament->official;
        $points = 0;
        if ($official) {
            if ($numPlayers == 64) {
                $points = 6400;
            } else if ($numPlayers == 32) {
                $points = 3200;
            } else if ($numPlayers == 16) {
                $points = 1600;
            } else if ($numPlayers == 8) {
                $points = 800;
            }
        }

        if ($round == 'Final') {
            $match = $infoUiT->match2;
            if ($match % 2 != 0) {
                $match += 1;
                $aux = $match;
            } else {
                $aux = $match - 1;
            }
            $infoUiT->round2 = true;
            $info->points += $points;
            $infoUiT2 = UsersInTournaments::where('tournament_id', '=', $tournamentId)->where('match2', '=', $aux)->first();
            $info2 = User::where('id', '=', $infoUiT2->user_id)->first();
            $infoUiT2->round2 = false;
            $info2->points += $points / 2;
        } else if ($round == 'Semifinales') {
            $match = $infoUiT->match4;
            if ($match % 2 != 0) {
                $match += 1;
                $aux = $match;
            } else {
                $aux = $match - 1;
            }
            $match /= 2;
            $infoUiT->match2 = $match;
            $infoUiT->round4 = true;
            $infoUiT2 = UsersInTournaments::where('tournament_id', '=', $tournamentId)->where('match4', '=', $aux)->first();
            $info2 = User::where('id', '=', $infoUiT2->user_id)->first();
            $infoUiT2->round4 = false;
            $info2->points += $points / 4;
        } else if ($round == 'Cuartos') {
            $match = $infoUiT->match8;
            if ($match % 2 != 0) {
                $match += 1;
                $aux = $match;
            } else {
                $aux = $match - 1;
            }
            $match /= 2;
            $infoUiT->match4 = $match;
            $infoUiT->round8 = true;
            $infoUiT2 = UsersInTournaments::where('tournament_id', '=', $tournamentId)->where('match8', '=', $aux)->first();
            $info2 = User::where('id', '=', $infoUiT2->user_id)->first();
            $infoUiT2->round8 = false;
            $info2->points += $points / 8;
        } else if ($round == 'Octavos') {
            $match = $infoUiT->match16;
            if ($match % 2 != 0) {
                $match += 1;
                $aux = $match;
            } else {
                $aux = $match - 1;
            }
            $match /= 2;
            $infoUiT->match8 = $match;
            $infoUiT->round16 = true;
            $infoUiT2 = UsersInTournaments::where('tournament_id', '=', $tournamentId)->where('match16', '=', $aux)->first();
            $info2 = User::where('id', '=', $infoUiT2->user_id)->first();
            $infoUiT2->round16 = false;
            $info2->points += $points / 16;
        } else if ($round == 'Ronda2') {
            $match = $infoUiT->match32;
            if ($match % 2 != 0) {
                $match += 1;
                $aux = $match;
            } else {
                $aux = $match - 1;
            }
            $match /= 2;
            $infoUiT->match16 = $match;
            $infoUiT->round32 = true;
            $infoUiT2 = UsersInTournaments::where('tournament_id', '=', $tournamentId)->where('match32', '=', $aux)->first();
            $info2 = User::where('id', '=', $infoUiT2->user_id)->first();
            $infoUiT2->round32 = false;
            $info2->points += $points / 32;
        } else if ($round == 'Ronda1') {
            $match = $infoUiT->match64;
            if ($match % 2 != 0) {
                $match += 1;
                $aux = $match;
            } else {
                $aux = $match - 1;
            }
            $match /= 2;
            $infoUiT->match32 = $match;
            $infoUiT->round64 = true;
            $infoUiT2 = UsersInTournaments::where('tournament_id', '=', $tournamentId)->where('match64', '=', $aux)->first();
            $info2 = User::where('id', '=', $infoUiT2->user_id)->first();
            $infoUiT2->round64 = false;
            $info2->points += $points / 64;
        }
        $infoUiT->save();
        $info->save();
        $infoUiT2->save();
        $info2->save();
        $this->conflict->oneConflict($tournamentId, $userId, $round)->delete();
        return redirect()->back();
    }
}
