<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Game;

use Illuminate\Http\Request;
use App\Http\Requests\EditProfileRequest;

use App\Services\UploadProfilePicService;

class ProfileController extends Controller
{
    public $user;

    public function __construct() {
        $this->user = new User;
        $this->game = new Game;
        $this->gameFilters = $this->game->gameFilters();
    }

    public function infoUser() {
        // Coger Usuario
        $userInfo = app('request')->session()->get('Usuario');
        $name = $userInfo['name'];
        $info = User::where('name', '=', $name)->first();

        return view('perfil')
        ->with("info", $info)
        ->with('gameFilters', $this->gameFilters);
    }

    public function userTournament($id) {
        $infoUser = User::where('id', '=', $id)->first();

        return view('perfil')
        ->with("infoUser", $infoUser)
        ->with('gameFilters', $this->gameFilters);
    }

    public function conf() {
        $userInfo = app('request')->session()->get('Usuario');
        $name = $userInfo['name'];
        $info = User::where('name', '=', $name)->first();

        return view('configuracion')
        ->with("info", $info)
        ->with('gameFilters', $this->gameFilters);
    }

    public function editProfile(EditProfileRequest $editProfileRequest, UploadProfilePicService $UploadProfilePicService) {
        // Coger Usuario
        $userInfo = app('request')->session()->get('Usuario');
        $name = $userInfo['name'];
        $info = User::where('name', '=', $name)->first();
        // Datos
        if ($editProfileRequest->filled('name')) {
            $newData = $editProfileRequest->input('name');
            $info->name = $newData;
        }
        if ($editProfileRequest->filled('password')) {
            $newData = $editProfileRequest->input('password');
            $info->password = $newData;
        }

        // Redes Sociales
        if ($editProfileRequest->filled('youtube')) {
            $newLink = $editProfileRequest->input('youtube');
            $info->youtube = $newLink;
        }
        if ($editProfileRequest->filled('twitch')) {
            $newLink = $editProfileRequest->input('twitch');
            $info->twitch = $newLink;
        }
        if ($editProfileRequest->filled('twitter')) {
            $newLink = $editProfileRequest->input('twitter');
            $info->twitter = $newLink;
        }
        if ($editProfileRequest->filled('instagram')) {
            $newLink = $editProfileRequest->input('instagram');
            $info->instagram = $newLink;
        }
        // Plataformas
        if ($editProfileRequest->filled('idps')) {
            $newId = $editProfileRequest->input('idps');
            $info->idps = $newId;
        }
        if ($editProfileRequest->filled('idxbox')) {
            $newId = $editProfileRequest->input('idxbox');
            $info->idxbox = $newId;
        }
        if ($editProfileRequest->filled('idepicgames')) {
            $newId = $editProfileRequest->input('idepicgames');
            $info->idepicgames = $newId;
        }
        if ($editProfileRequest->filled('idswitch')) {
            $newId = $editProfileRequest->input('idswitch');
            $info->idswitch = $newId;
        }
        if ($editProfileRequest->filled('idsteam')) {
            $newId = $editProfileRequest->input('idsteam');
            $info->idsteam = $newId;
        }
        if ($editProfileRequest->filled('idriotgames')) {
            $newId = $editProfileRequest->input('idriotgames');
            $info->idriotgames = $newId;
        }
        if ($editProfileRequest->filled('idblizzard')) {
            $newId = $editProfileRequest->input('idblizzard');
            $info->idblizzard = $newId;
        }
        if ($editProfileRequest->filled('idsupercell')) {
            $newId = $editProfileRequest->input('idsupercell');
            $info->idsupercell = $newId;
        }
        // Imagen Perfil
        if ($editProfileRequest->file('img')) {
            $this->uploadService = $UploadProfilePicService;
            $this->uploadService->uploadFile($editProfileRequest->file('img'));
            $newImg = $editProfileRequest->img->getClientOriginalName();
            $info->img = $newImg;
        }
        // Imagen fondo
        if ($editProfileRequest->filled('bannerPerfil')) {
            $newBanner = $editProfileRequest->input('bannerPerfil');
            //$userInfo['backImg'] = $newBanner;
            $info->backImg = $newBanner;
        }
        $info->save();

        return redirect('perfil')
        ->with("info", $info)
        ->with('gameFilters', $this->gameFilters);
    }
}
