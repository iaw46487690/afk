<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PrizeRequest;
use App\Models\Prize;
use App\Models\Game;
use App\Models\User;
use Exception;

class PrizeController extends Controller
{
    public $prize;

    public function __construct() {
        $this->prize = new Prize;
        $this->game = new Game;
        $this->gameFilters = $this->game->gameFilters();
    }

    /**
     * Muestra todos los premios
     */
    public function showPrizePage() {
        $prizes = $this->prize->allPrizes()->get();

        try {
            $userInfo = app('request')->session()->get('Usuario');
            $name = $userInfo['name'];
            $info = User::where('name', '=', $name)->first();

            return view('puntos')
            ->with("info", $info)
            ->with('prizes', $prizes)
            ->with('gameFilters', $this->gameFilters);
        } catch (Exception $e) {
            return view('puntos')
            ->with('prizes', $prizes)
            ->with('gameFilters', $this->gameFilters);
        }
    }

    /**
     * Muestra la pagina de 1 premio
     */
    public function showPrize($prize) {
        $prizes = $this->prize->onePrize($prize)->first();
        $userInfo = app('request')->session()->get('Usuario');
        $name = $userInfo['name'];
        $info = User::where('name', '=', $name)->first();

        return view('premio')
        ->with("info", $info)
        ->with('prize', $prizes)
        ->with('gameFilters', $this->gameFilters);
    }

    /**
     * Muestra la pagina del recibo
     */
    public function showRecibo(PrizeRequest $prizeRequest) {
        $userInfo = app('request')->session()->get('Usuario');
        $name = $userInfo['name'];
        $info = User::where('name', '=', $name)->first();
        $newPoints = $info->points - $prizeRequest->input('points');
        $info->points = $newPoints;
        $info->save();

        return view('recibo')
        ->with('gameFilters', $this->gameFilters);
    }
}
