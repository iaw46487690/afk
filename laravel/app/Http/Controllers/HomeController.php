<?php

namespace App\Http\Controllers;
use App\Models\Game;
use App\Models\TournamentInfo;
use App\Models\Platform;

use App\Http\Requests\FilterRequest;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public $tournament;
    public $platform;

    public function __construct() {
        $this->tournament = new TournamentInfo;
        $this->game = new Game;
        $this->platform = new Platform;
        $this->gameFilters = $this->game->gameFilters();
    }

    public function inicio() {
        $tournaments = $this->tournament->date()->get();
        $platforms = $this->platform->get();

        return view('principal')
        ->with('tournaments', $tournaments)
        ->with('tournamentsPlatform', $tournaments)
        ->with('platforms', $platforms)
        ->with('gameFilters', $this->gameFilters);
    }

    public function filter(FilterRequest $request, $platformName) {
        $request->flash();
        $tournaments = $this->tournament->query();
        $tournamentsDate = $tournaments->date()->get();
        $tournaments->date();
        $diferentPlatforms = $this->platform->get();
        $platforms = [];
        //dd($platformName);
        if ($platformName != "Todos") {
            $platforms = [];
            if ($platformName == "PlayStation") {
                array_push($platforms, "PlayStation");
            }
            if ($platformName == "XBOX") {
                array_push($platforms, "XBOX");
            }
            if ($platformName == "PC") {
                array_push($platforms, "PC");
            }
            if ($platformName == "Nintendo switch") {
                array_push($platforms, "Nintendo switch");
            }
            if ($platformName == "Movil") {
                array_push($platforms, "Movil");
            }
            $tournaments->platformFilterArray($platforms);
        }
        return view('principal')
        ->with('tournamentsPlatform', $tournaments->get())
        ->with('tournaments', $tournamentsDate)
        ->with('platforms', $diferentPlatforms)
        ->with('gameFilters', $this->gameFilters);
    }

    /*public function filterPlatform(FilterRequest $request) {
        $request->flash();
        $tournaments = $this->tournament->query();
        $tournamentsDate = $tournaments->date()->get();
        $tournaments->date();
        // Filtro plataformas
        if (!$request->filled('Todos')) {
            $platforms = [];
            if ($request->filled('PlayStation')) {
                array_push($platforms, "PlayStation");
            }
            if ($request->filled('XBOX')) {
                array_push($platforms, "XBOX");
            }
            if ($request->filled('PC')) {
                array_push($platforms, "PC");
            }
            if ($request->filled('NintendoSwitch')) {
                array_push($platforms, "Nintendo switch");
            }
            if ($request->filled('Movil')) {
                array_push($platforms, "Movil");
            }
            if ($platforms[0] != "Movil") {
                array_push($platforms, "Todas las plataformas");
            }
            $tournaments->platformFilterArray($platforms);
        }
        return view('principal')->with('tournamentsPlatform', $tournaments->get())->with('tournaments', $tournamentsDate)
        ->with('gameFilters', $this->gameFilters);
    }

    public function filterPlayStation(FilterRequest $request) {
        $request->flash();
        $tournaments = $this->tournament->query();
        $tournamentsDate = $tournaments->date()->get();
        $tournaments->date();
        $platforms = [];
        array_push($platforms, "PlayStation");
        array_push($platforms, "Todas las plataformas");
        $tournaments->platformFilterArray($platforms);
        return view('principal')->with('tournamentsPlatform', $tournaments->get())->with('tournaments', $tournamentsDate)
        ->with('gameFilters', $this->gameFilters);
    }

    public function filterXbox(FilterRequest $request) {
        $request->flash();
        $tournaments = $this->tournament->query();
        $tournamentsDate = $tournaments->date()->get();
        $tournaments->date();
        $platforms = [];
        array_push($platforms, "XBOX");
        array_push($platforms, "Todas las plataformas");
        $tournaments->platformFilterArray($platforms);
        return view('principal')->with('tournamentsPlatform', $tournaments->get())->with('tournaments', $tournamentsDate)
        ->with('gameFilters', $this->gameFilters);
    }*/
}
