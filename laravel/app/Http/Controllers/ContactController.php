<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Game;

class ContactController extends Controller
{
    public function __construct() {
        $this->game = new Game;
        $this->gameFilters = $this->game->gameFilters();
    }

    public function showContactPage() {

        return view('contacto')
        ->with('gameFilters', $this->gameFilters);
    }
}
