<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Game;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
//use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public $user;

    public function __construct() {
        $this->user = new User;
        $this->game = new Game;
        $this->gameFilters = $this->game->gameFilters();
    }

    public function getLogin() {
        return view('login')
        ->with('gameFilters', $this->gameFilters);
    }

    public function getRegister() {
        return view('registro')
        ->with('gameFilters', $this->gameFilters);
    }

    public function closeSession(Request $request) {
        $request->session()->forget('Usuario');

        return redirect('/')
        ->with('gameFilters', $this->gameFilters);
    }

    public function loginUser(LoginRequest $request) {
        $email = $request->input('email');
        $password = $request->input('password');
        if ($password != $this->user->loginPassword($email)->value('password')) {
            return redirect()->back()
            ->with('user', $this->user->validate($email, $password)->get())
            ->with('gameFilters', $this->gameFilters);
        }
        $name = $this->user->loginName($email)->value('name');
        $type = $this->user->typeUser($email)->value('type');
        $arrayUser = array('name' => $name, 'email' => $email, 'type' => $type);
        $request->session()->put('Usuario', $arrayUser);
        //dd(app('request')->session()->get('_token'));
        // Si el usuario es admin redirigimos a misTorneos
        if ($type == 'admin') {
            return redirect('/misTorneos');
        } else {
            return redirect('/')
            ->with('gameFilters', $this->gameFilters);
        }
    }

    public function registerUser(RegisterRequest $request) {
        $newUser = new User;
        $newUser->name = $request->input('name');
        $newUser->email = $request->input('email');
        $newUser->password = $request->input('password');
        $repeatPassword = $request->input('password_confirmation');
        $newUser->save();
        $type = $this->user->typeUser($newUser->email)->value('type');
        $arrayUser = array('name' => $newUser->name, 'email' => $newUser->email, 'type' => $type);
        $request->session()->put('Usuario', $arrayUser);

        return redirect('/')
        ->with(['user'=>$this->user->validate($newUser->name, $newUser->email, $newUser->password, $repeatPassword)->get()])
        ->with('gameFilters', $this->gameFilters);
    }
}
