<?php

namespace App\Http\Controllers;
use App\Models\Game;
use App\Models\TournamentInfo;
use App\Models\User;

use Illuminate\Http\Request;

class MyTournamentController extends Controller
{
    public $tournament;

    public function __construct() {
        $this->tournament = new TournamentInfo;
        $this->game = new Game;
        $this->gameFilters = $this->game->gameFilters();
    }

    public function showMyTournament() {
        $userInfo = app('request')->session()->get('Usuario');
        $name = $userInfo['name'];
        $info = User::where('name', '=', $name)->first();
        $userId = $info->id;

        if ($userInfo['type'] == 'estandar') {
            $oldTournaments = $this->tournament->oldTournaments($userId)->get();
            $nextTournaments = $this->tournament->nextTournaments($userId)->get();

            return view('misTorneos')
            ->with('oldTournaments', $oldTournaments)
            ->with('nextTournaments', $nextTournaments)
            ->with('gameFilters', $this->gameFilters);
        } else {
            $oldTournaments = $this->tournament->oldTournamentsAdmin($userId)->get();
            $nextTournaments = $this->tournament->nextTournamentsAdmin($userId)->get();
            $ids = $this->tournament->conflictTournaments($userId)->get();
            $tournaments_ids = array();
            foreach ($ids as $aux) {
                array_push($tournaments_ids, $aux->tournament_id);
            }
            $conflictTournaments = $this->tournament->getConflictTournaments($tournaments_ids)->get();

            return view('misTorneos')
            ->with('oldTournaments', $oldTournaments)
            ->with('nextTournaments', $nextTournaments)
            ->with('conflictTournaments', $conflictTournaments)
            ->with('gameFilters', $this->gameFilters);
        }
    }
}
