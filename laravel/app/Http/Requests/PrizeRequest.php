<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PrizeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|max:20',
            'apellidos' => 'required|max:30',
            'email' => 'required|email',
            'telefono' => 'required|numeric',
            'direccion' => 'required',
            'ciudad' => 'required',
            'codigoPostal' => 'required|digits:5|numeric',
            'pais' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El campo :attribute és obligatorio',
            'nombre.max' => 'El campo :attribute no puede ser mayor de 20 carácteres',
            'apellidos.required' => 'El campo :attribute és obligatorio',
            'apellidos.max' => 'El campo :attribute no puede ser mayor de 30 carácteres',
            'email.required' => 'El campo :attribute és obligatorio',
            'email.email' => 'El campo :attribute no és una dirección de correo valida',
            'telefono.required' => 'El campo :attribute és obligatorio',
            'telefono.numeric' => 'El campo :attribute no puede contener letras',
            'pais.required' => 'El campo :attribute és obligatorio',
            'ciudad.required' => 'El campo :attribute és obligatorio',
            'codigoPostal.required' => 'El campo :attribute és obligatorio',
            'codigoPostal.digits' => 'El campo :attribute ha de tener :digits digitos.',
            'codigoPostal.numeric' => 'El campo :attribute no puede contener letras',
            'direccion.required' => 'El campo :attribute és obligatorio',
        ];
    }
}
