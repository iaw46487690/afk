<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTournamentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'juego' => 'required',
            'date' => 'required',
            'hour' => 'required',
            'equipo' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Escoge un nombre para el torneo',
            'juego.required' => 'Elige el juego del torneo',
            'date.required' => 'Selecciona una fecha para el torneo',
            'hour.required' => 'Selecciona una hora para el torneo',
            'equipo.required' => 'Selecciona una opción, \'Inidvidual\' o \'Por equipos\'',
        ];
    }
}
