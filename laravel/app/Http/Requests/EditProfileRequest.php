<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'confirmed|min:6|nullable',
            'youtube' => 'active_url|nullable',
            'twitch' => 'active_url|nullable',
            'twitter' => 'active_url|nullable',
            'instagram' => 'active_url|nullable',
            'img' => 'mimes:jpg,png|nullable',
        ];
    }

    public function messages()
    {
        return [
            'password.confirmed' => 'La contraseña no coincide',
            'password.min' => 'La contraseña tiene que tener almenos 6 carácteres',
            'youtube.active_url' => 'El link de :attribute no és una dirección URL valida',
            'twitch.active_url' => 'El link de :attribute no és una dirección URL valida',
            'twitter.active_url' => 'El link de :attribute no és una dirección URL valida',
            'instagram.active_url' => 'El link de :attribute no és una dirección URL valida',
            'img.mimes' => 'La imagen tiene que ser un fichero de tipo .jpg o .png',
        ];
    }
}
