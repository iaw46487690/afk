<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'El camp email és obligatori.',
            'email.email' => 'El camp email ha de ser una direcció de email vàlida.',
            'password.required' => 'El camp contrasenya és obligatori.',
            'password.min' => 'El camp contrasenya ha de tindre com a mínim 6 caràcters.',
        ];
    }
}
