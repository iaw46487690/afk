<?php

namespace App\Services;

class UploadProfilePicService
{

    function uploadFile($file)
    {
        $destinationPath = 'img/profileImg/';
        $originalFile = $file->getClientOriginalName();
        $file->move($destinationPath, $originalFile);
    }
}
