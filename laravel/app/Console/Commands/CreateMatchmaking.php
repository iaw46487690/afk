<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\TournamentInfo;
use App\Models\UsersInTournaments;
use Carbon\Carbon;

class CreateMatchmaking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:matchmaking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->tournament = new TournamentInfo;
        $this->userTournament = new UsersInTournaments;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $x = 0;
        $emparejamientos = array();
        $defaultName = '- - -';
        $tournaments = $this->tournament->sameDate()->get();

        foreach ($tournaments as $tournament) {
            $emparejamientos[$x] = array();
            $matches = $this->userTournament->tournament($tournament->id)->get();

            if (count($matches) >= $tournament->capacity / 2) {
                for ($i=0; $i < $tournament->capacity; $i++) {
                    if (isset($matches[$i]->user_id)) {
                        $user = User::where('id', '=', $matches[$i]->user_id)->first();
                        $user = $user->id;
                    } else {
                        $user = $defaultName;
                    }
                    array_push($emparejamientos[$x], $user);
                }
                shuffle($emparejamientos[$x]);
            }
            $x++;
        }
        $x = 0;
        foreach ($emparejamientos as $torneo) {
            $matchNumber = 1;
            $countTorneo = count($torneo);
            for ($i=0; $i < $countTorneo; $i++) {
                if ($torneo[$i] != $defaultName) {
                    $userInTournament = $this->userTournament->playing($torneo[$i], $tournaments[$x]->id)->first();
                } else {
                    $userInTournament = new UsersInTournaments;
                    $userInTournament->tournament_id = $tournaments[$x]->id;
                    $userInTournament->user_id = null;
                }
                if ($countTorneo == 64) {
                    $userInTournament->match64 = $matchNumber;
                } elseif ($countTorneo == 32) {
                    $userInTournament->match32 = $matchNumber;
                } elseif ($countTorneo == 16) {
                    $userInTournament->match16 = $matchNumber;
                } elseif ($countTorneo == 8) {
                    $userInTournament->match8 = $matchNumber;
                }
                $userInTournament->save();
                $matchNumber++;
            }
            $x++;
        }
        $this->info('Emparejamientos realizados en ' . $x . ' torneos');
        return 1;
    }
}
