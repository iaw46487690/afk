<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class UsersInTournaments extends Model
{
    use HasFactory;

    protected $table = 'usersintournaments';

    public function scopePlaying($query, $user_id, $tournament_id) {
        return $query->where('user_id', '=', $user_id)->where('tournament_id', '=', $tournament_id);
    }

    public function scopeExpired($query, $user_id, $tournament_id) {
        $current_date = Carbon::now();
        return $query->rightjoin('tournament_infos', 'tournament_infos.id', '=', 'usersintournaments.tournament_id')
        ->where('date', '<', $current_date)
        ->where('usersintournaments.user_id', '=', $user_id)
        ->where('usersintournaments.tournament_id', '=', $tournament_id);
    }

    public function scopeExpiredAdmin($query, $user_id, $tournament_id) {
        $current_date = Carbon::now();
        return $query->rightjoin('tournament_infos', 'tournament_infos.id', '=', 'usersintournaments.tournament_id')
        ->where('date', '<', $current_date)
        ->where('tournament_infos.user_id', '=', $user_id)
        ->where('usersintournaments.tournament_id', '=', $tournament_id);
    }

    // Cron
    public function scopeSameDate($query) {
        $current_date = Carbon::now()->format('Y-m-d H:i');
        return $query->join('tournament_infos', 'tournament_infos.id', '=', 'usersintournaments.tournament_id')
        ->where('date', '=', $current_date);
    }

    public function scopeTournament($query, $tournament_id) {
        return $query->where('tournament_id', '=', $tournament_id);
    }

    public function scopeOrderByMatch64($query) {
        return $query->select('tournament_id',  'user_id', 'username', 'round64 as winner')
        ->orderBy('match64')
        ->limit(64);;
    }

    public function scopeOrderByMatch32($query) {
        return $query->select('tournament_id',  'user_id', 'username', 'round32 as winner')
        ->where('round64', true)
        ->orderBy('match32')
        ->limit(32);;
    }

    public function scopeOrderByFirstMatch32($query) {
        return $query->select('tournament_id',  'user_id', 'username', 'round32 as winner')
        ->orderBy('match32')
        ->limit(32);
    }

    public function scopeOrderByMatch16($query) {
        return $query->select('tournament_id',  'user_id', 'username', 'round16 as winner')
        ->where('round32', true)
        ->orderBy('match16')
        ->limit(16);
    }

    public function scopeOrderByFirstMatch16($query) {
        return $query->select('tournament_id',  'user_id', 'username', 'round16 as winner')
        ->orderBy('match16')
        ->limit(16);
    }

    public function scopeOrderByMatch8($query) {
        return $query->select('tournament_id',  'user_id', 'username', 'round8 as winner')
        ->where('round16', true)
        ->orderBy('match8')
        ->limit(8);
    }

    public function scopeOrderByFirstMatch8($query) {
        return $query->select('tournament_id',  'user_id', 'username', 'round8 as winner')
        ->orderBy('match8')
        ->limit(8);
    }

    public function scopeOrderByMatch4($query) {
        return $query->select('tournament_id',  'user_id', 'username', 'round4 as winner')
        ->where('round8', true)
        ->orderBy('match4')
        ->limit(4);
    }

    public function scopeOrderByMatch2($query) {
        return $query->select('tournament_id',  'user_id', 'username', 'round2 as winner')
        ->where('round4', true)
        ->orderBy('match2')
        ->limit(2);
    }
}
