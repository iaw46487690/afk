<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class TournamentInfo extends Model
{
    use HasFactory;

    public function scopeGameFilter($query, $game) {
        return $query->where('game_id', '=', $game );
    }

    public function scopeOfficial($query,$input) {
        return $query->where('official',$input);
    }

    public function scopeSearch($query, $search) {
        return $query->where('name', 'LIKE', '%' . $search . '%');
    }

    public function scopePlatformFilterArray($query, $platforms) {
        return $query->whereIn('platform', $platforms);
    }

    public function scopeGetTournamentById($query, $id) {
        return $query->where('id', '=', $id);
    }

    public function scopeDate($query) {
        $current_date = Carbon::now();
        return $query->where('date', '>=', $current_date)
        ->orderBy('date', 'ASC');
    }

    public function scopeNextTournaments($query, $userId) {
        $current_date = Carbon::now();
        return $query->rightjoin('usersintournaments', 'tournament_infos.id', '=', 'usersintournaments.tournament_id')
        ->where('date', '>=', $current_date)
        ->where('usersintournaments.user_id', '=', $userId)
        ->orderBy('date', 'ASC');
    }

    public function scopeOldTournaments($query, $userId) {
        $current_date = Carbon::now();
        return $query->rightjoin('usersintournaments', 'tournament_infos.id', '=', 'usersintournaments.tournament_id')
        ->where('date', '<', $current_date)
        ->where('usersintournaments.user_id', '=', $userId)
        ->orderBy('date', 'DESC');
    }

    public function scopeNextTournamentsAdmin($query, $userId) {
        $current_date = Carbon::now();
        return $query->where('date', '>=', $current_date)
        ->where('user_id', '=', $userId)
        ->orderBy('date', 'ASC');
    }

    public function scopeOldTournamentsAdmin($query, $userId) {
        $current_date = Carbon::now();
        return $query->where('date', '<', $current_date)
        ->where('user_id', '=', $userId)
        ->orderBy('date', 'DESC');
    }

    public function scopeConflictTournaments($query, $userId) {
        return $query->select('conflicts.tournament_id')
        ->rightjoin('conflicts', 'tournament_infos.id', '=', 'conflicts.tournament_id')
        ->where('user_id', '=', $userId)
        ->groupBy('conflicts.tournament_id');
    }

    public function scopeGetConflictTournaments($query, $tournament_ids) {
        return $query->whereIn('id', $tournament_ids)
        ->orderBy('date', 'DESC');
    }

    public function scopeSameDate($query) {
        $current_date = Carbon::now()->format('Y-m-d H:i');
        return $query->select(DB::raw('DISTINCT(tournament_infos.id), capacity'))
        ->join('usersintournaments', 'tournament_infos.id', '=', 'usersintournaments.tournament_id')
        ->where('date', '=', $current_date);
    }
}
