<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class User extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    public function scopeLoginPassword($query, $user) {
        return $query -> select('password')->where('email', 'LIKE', '%' . $user . '%');
    }

    public function scopeLoginName($query, $user) {
        return $query -> select('name')->where('email', 'LIKE', '%' . $user . '%');
    }

    public function scopetypeUser($query, $user) {
        return $query -> select('type')->where('email', 'LIKE', '%' . $user . '%');
    }

    public function scopeValidate($query, $user) {
        return $query -> where('name', 'LIKE', '%' . $user . '%')
            ->orWhere('email', 'LIKE', '%' . $user . '%')
            ->orWhere('password', 'LIKE', '%' . $user . '%');
    }
}
