<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prize extends Model
{
    use HasFactory;

    public function scopeAllPrizes($query) {
        return $query->orderBy('points', 'desc')->orderBy('name', 'asc');
    }

    public function scopeOnePrize($query, $prize) {
        return $query->where('id', '=', $prize);
    }
}
