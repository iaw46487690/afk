<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Conflict extends Model
{
    use HasFactory;

    public function scopeConflicts($query, $tournamentId) {
        return $query->where('tournament_id', '=', $tournamentId);
    }

    public function scopeOneConflict($query, $tournamentId, $userId, $round) {
        return $query->where('tournament_id', '=', $tournamentId)
        ->where('round', '=', $round)
        ->where('user_id_1', '=', $userId)
        ->orWhere('user_id_2', '=', $userId);
    }
}
