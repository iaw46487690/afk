<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    use HasFactory;

    public function scopeGameFilters($query) {
        return $query->select('id', 'name', 'img', 'video')->orderBy('name', 'asc')->get();
    }

    public function scopeGameInfo($query, $game) {
        return $query->where('id', '=', $game);
    }

    public function scopeFindIdGame($query, $game) {
        return $query->where('name', '=', $game);
    }
}
