<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PrizeController;
use App\Http\Controllers\TournamentController;
use App\Http\Controllers\MyTournamentController;
use App\Http\Controllers\ContactController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Juegos
Route::get('/juegos', [GameController::class, "filterGames"]);
Route::get('/juegos/{game}', [GameController::class, "showGamePage"]);
Route::post('/juegos/filter', [GameController::class, "filterGames"]);
Route::post('/search', [GameController::class, "filterGames"]);

// Puntos
Route::get('/puntos', [PrizeController::class, "showPrizePage"]);
Route::get('/puntos/{prize}', [PrizeController::class, "showPrize"]);

// Recibo
Route::post('/recibo', [PrizeController::class, "showRecibo"]);

// Login & Register
Route::get('/login', [UserController::class, "getLogin"])->name('login');
Route::post('/login', [UserController::class, "loginUser"]);
Route::get('/register', [UserController::class, "getRegister"]);
Route::post('/register', [UserController::class, "registerUser"]);
Route::get('/logout', [UserController::class, "closeSession"]);

// Inicio
Route::get('/', [HomeController::class, "inicio"]);
Route::post('/inicio/{platform}', [HomeController::class, "filter"]);

// Profile
Route::get('/perfil', [ProfileController::class, "infoUser"])->middleware('estandarSession');
Route::get('/perfil/{id}', [ProfileController::class, "userTournament"]);

// Configuracion
Route::get('/configuracion', [ProfileController::class, "conf"])->middleware('estandarSession');
Route::post('/configuracion', [ProfileController::class, "editProfile"]);

// Crear Torneo
Route::get('/crearTorneo', [TournamentController::class, "showCreateTournament"])->middleware('estandarSession');
Route::post('/crearTorneo', [TournamentController::class, "createNewTournament"]);

// Torneo
Route::get('/torneo/{tournament}', [TournamentController::class, "showTournament"]);
Route::get('/torneo/{tournament}/conflictos', [TournamentController::class, "showConflicts"]);
Route::get('/registro/{tournament}', [TournamentController::class, "registerTournament"]);
Route::get('/salir/{tournament}', [TournamentController::class, "removeFromTournament"]);
Route::get('/looser/{id}', [TournamentController::class, "looser"]);
Route::get('/winner/{id}', [TournamentController::class, "winner"]);
Route::get('/winnerConflict/{id}', [TournamentController::class, "winnerConflict"]);

// Mis Torneos
Route::get('/misTorneos', [MyTournamentController::class, "showMyTournament"])->middleware('estandarSession');

// Crear Torneo
Route::get('/contacto', [ContactController::class, "showContactPage"]);

