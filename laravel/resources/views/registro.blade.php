<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Amin Template">
        <meta name="keywords" content="Amin, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/icon.png')}}">
        <link rel="icon" type="image/png" href="{{asset('img/icon.png')}}">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- Title -->
        <title>Registro</title>
        <!-- Title -->

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&display=swap"
            rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Cinzel:400,700,900&display=swap" rel="stylesheet">

        <!-- Compiled and minified Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >

        <!-- Css Styles -->
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('css/elegant-icons.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('css/barfiller.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('css/slicknav.min.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('css/torneo.css')}}" type="text/css">
        <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
    </head>
    <body>
        <div class="signup-text" style="background-color: #0d0d0d">
            <a href="/">
                <div class="return">
                    <span class="iconify" data-icon="bi:arrow-left-circle" data-inline="false"></span>&nbsp;&nbsp;
                    <i class="fa fa-home" style="color: white">&nbsp;&nbsp;Inicio</i>
                </div>
            </a>
            <div class="container">
                <br><br>
                <div class="signup-title">
                    <h2>Registrarse</h2>
                </div>
                <form class="signup-form" action="/register" method="POST">
                    @csrf
                    @if ($errors->any())
                        <div class="alert" style="color:#c20000; font-size:16px">
                            @foreach($errors->all() as $error)
                                {{ $error }} <br>
                            @endforeach
                            <br>
                        </div>
                    @endif
                    <div class="sf-input-list">
                        <input type="text" class="input-value" name="name" placeholder="* Nombre de usuario" value={{ old('name') }}>
                        <input type="text" class="input-value" name="email" placeholder="* Correo electronico" value={{ old('email') }}>
                        <input type="password" class="input-value" name="password" placeholder="* Contraseña">
                        <input type="password" class="input-value" name="password_confirmation" placeholder="* Confirmar Contraseña">
                    </div>
                    <br>
                    <a href="/login">
                        <div class="registerToLogin text-center">
                            <span class="iconify username" style="font-size:15px" data-icon="akar-icons:arrow-left" data-inline="false"></span>&nbsp;&nbsp;Login
                        </div>
                    </a>
                    <br><br>
                    <button type="submit"><span>REGISTRARSE</span></button>
                </form>
            </div>
        </div>
        <!-- Sign Up Section End -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
        <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('js/circle-progress.min.js')}}"></script>
        <script src="{{asset('js/jquery.barfiller.js')}}"></script>
        <script src="{{asset('js/jquery.slicknav.js')}}"></script>
        <script src="{{asset('js/owl.carousel.min.js')}}"></script>
        <script src="{{asset('js/main.js')}}"></script>
    <body>
</html>
