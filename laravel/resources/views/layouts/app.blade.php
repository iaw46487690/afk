<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Amin Template">
    <meta name="keywords" content="Amin, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/icon.png')}}">
    <link rel="icon" type="image/png" href="{{asset('img/icon.png')}}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Title -->
    @yield('title')
    <!-- Title -->

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cinzel:400,700,900&display=swap" rel="stylesheet">

    <!-- Compiled and minified Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/barfiller.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/torneo.css')}}" type="text/css">
    <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Humberger Menu Begin -->
    @php
        $user = app('request')->session()->get('Usuario');
    @endphp
    @if ($user)
        @if ($user['type'] != 'admin')
            <div class="humberger-menu-overlay"></div>
            <div class="humberger-menu-wrapper">
                <div class="hw-logo">
                    <a href="/"><img class="img" src="{{asset('img/logo.png')}}" alt=""></a>
                </div>
                <div class="hw-menu mobile-menu">
                    <ul style="text-align:center">
                    <h2 style="color:white">JUEGOS</h2><br>
                    @forelse ($gameFilters as $game)
                        <li><a href="/juegos/{{$game->id}}"><img class="img" src="{{asset('img/games/').'/'.$game->img}}" alt="{{$game->name}}"><br><br>{{ $game->name }}</a></li>
                    @empty
                        <h4 class="username">No hay juegos disponibles</h4>
                    @endforelse
                    </ul>
                </div>
                <div id="mobile-menu-wrap"></div>
            </div>
        @endif
    @else
        <div class="humberger-menu-overlay"></div>
        <div class="humberger-menu-wrapper">
            <div class="hw-logo">
                <a href="/"><img class="img" src="{{asset('img/logo.png')}}" alt=""></a>
            </div>
            <div class="hw-menu mobile-menu">
                <ul style="text-align:center">
                <h2 style="color:white">JUEGOS</h2><br>
                @forelse ($gameFilters as $game)
                    <li><a href="/juegos/{{$game->id}}"><img class="img" src="{{asset('img/games/').'/'.$game->img}}" alt="{{$game->name}}"><br><br>{{ $game->name }}</a></li>
                @empty
                    <h4 class="username">No hay juegos disponibles</h4>
                @endforelse
                </ul>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    @endif
    <!-- Humberger Menu End -->

    <!-- Header Section Begin -->
    <header class="header-section">
        <div class="ht-options">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="ht-widget htw1">
                          <script>
                              const monthNames = ["January", "February", "March", "April", "May", "June",
                                      "July", "August", "September", "October", "November", "December"];
                              const d = new Date();
                          </script>
                            <ul>
                                <li><i class="fa fa-clock-o"></i>
                                  <script>document.write(monthNames[d.getMonth()] + " " + new Date().getDate() + ", " + new Date().getFullYear());</script></li>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="ht-widget htw2">
                          <ul>
                            @php /*var_dump ($user);*/ @endphp
                            @if ($user)
                                <li><b> {{ $user['name'] }} </b></li>
                                <!--<li><a href="/logout"><b> Cerrar Sesion </b></a></li>-->
                                <li class="closeSession"><b> Cerrar Sesión </b></li>
                            @else
                                <li style="cursor:pointer"><a href="/login" class="username"><b> Iniciar Sesión </b></a></li>
                                <li style="cursor:pointer"><a href="/register" class="username"><b> Registrarse </b></a></li>
                            @endif
                          </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="logo">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                       <a href="/"><img class="imgLogo" src="{{asset('img/logo.png')}}" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav-options">
            <div class="container">
                @if ($user)
                    @if ($user['type'] != 'admin')
                        <div class="humberger-menu humberger-open">
                            <i class="fa fa-bars">&nbsp;&nbsp;JUEGOS</i>
                        </div>
                        <div class="nav-search">
                            <form class="search-model-form2" action="/search" method="post">
                                @csrf
                                <input type="text" name="search" placeholder="Buscar . . ." oninvalid="this.setCustomValidity('Completa este campo')" required>
                            </form>
                        </div>
                    @endif
                @else
                    <div class="humberger-menu humberger-open">
                        <i class="fa fa-bars">&nbsp;&nbsp;JUEGOS</i>
                    </div>
                    <div class="nav-search">
                        <form class="search-model-form2" action="/search" method="post">
                            @csrf
                            <input type="text" name="search" placeholder="Buscar . . ." oninvalid="this.setCustomValidity('Completa este campo')" required>
                        </form>
                    </div>
                @endif
                <div class="nav-menu ">
                    <ul>
                        @if ($user)
                            @if ($user['type'] != 'admin')
                                <li class="active"><a href="/"><span class="fa fa-home">&nbsp;&nbsp;Inicio</span></a></li>
                                <li>
                                    <a href="/perfil">
                                        <span class="fa fa-user">&nbsp;&nbsp;Mi Perfil</span>
                                    </a>
                                </li>
                            @endif
                        @else
                            <li class="active"><a href="/"><span class="fa fa-home">&nbsp;&nbsp;Inicio</span></a></li>
                            <li>
                                <a href="/login" style="cursor:pointer">
                                    <span class="fa fa-user">&nbsp;&nbsp;Mi Perfil</span>
                                </a>
                            </li>
                        @endif
                        <li>
                            @if ($user)
                                <a href="/misTorneos">
                                    <span class="fa fa-list-ul">&nbsp;&nbsp;Mis Torneos</span>
                                </a>
                            @else
                                <a href="/login" style="cursor:pointer">
                                    <span class="fa fa-user">&nbsp;&nbsp;Mis Torneos</span>
                                </a>
                            @endif
                        </li>
                        <li>
                            @if ($user)
                                <a href="/crearTorneo">
                                    <span class="fa fa-plus-circle">&nbsp;&nbsp;Crear Torneo</span>
                                </a>
                            @else
                                <a href="/login" style="cursor:pointer">
                                    <span class="fa fa-user">&nbsp;&nbsp;Crear Torneo</span>
                                </a>
                            @endif
                        </li>
                        @if ($user)
                            @if ($user['type'] != 'admin')
                                <li><a href="/puntos"><span class="fa fa-product-hunt">&nbsp;&nbsp;Puntos</span></a></li>
                            @endif
                        @else
                            <li><a href="/puntos"><span class="fa fa-product-hunt">&nbsp;&nbsp;Puntos</span></a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Content -->
    @yield('content')
    <!-- Content -->

    <!-- Footer Section Begin -->
    <footer class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="footer-about">
                        <div class="fa-logo">
                            <img class="img" src="{{asset('img/logo.png')}}" alt="">
                        </div>
                        <p>AFK, la web de torneos donde conseguir premios es posible</p>
                        <div class="sidebar-option2">
                            <div class="social-media2">
                                <ul>
                                    <a href="https://www.youtube.com/" target="_blank">
                                        <li class="youtube">
                                            <div class="sm-icon"><i class="fa fa-youtube-play"></i></div>
                                        </li>
                                    </a>
                                    <a href="https://www.twitch.tv/" target="_blank">
                                        <li class="twitch">
                                            <div class="sm-icon"><i class="fa fa-twitch"></i></div>
                                        </li>
                                    </a>
                                    <a href="https://twitter.com/" target="_blank">
                                        <li class="twitter">
                                            <div class="sm-icon"><i class="fa fa-twitter"></i></div>
                                        </li>
                                    </a>
                                    <a href="https://www.instagram.com/" target="_blank">
                                        <li class="instagram">
                                            <div class="sm-icon"><i class="fa fa-instagram"></i></div>
                                        </li>
                                    </a>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6">
                    <div class="tags-cloud">
                        <div class="section-title">
                            <h5>Videojuegos</h5>
                        </div>
                        <div class="tag-list">
                            <a href="https://supercell.com/en/games/brawlstars/" target="_blank"><span>Brawl Stars</span></a>
                            <a href="https://supercell.com/en/games/clashroyale/" target="_blank"><span>Clash Royale</span></a>
                            <a href="https://blog.counter-strike.net/" target="_blank"><span>CSGO</span></a>
                            <a href="https://www.ea.com/es-es/games/fifa/fifa-21" target="_blank"><span>Fifa</span></a>
                            <a href="https://playhearthstone.com/es-es" target="_blank"><span>Heartstone</span></a>
                            <a href="https://na.leagueoflegends.com/es-es/" target="_blank"><span>LOL</span></a>
                            <a href="https://mariokart8.nintendo.com/es/" target="_blank"><span>Mario Kart 8</span></a>
                            <a href="https://2k.com/en-US/game/nba-2k21-mamba-forever-edition-next-gen/" target="_blank"><span>NBA 2K</span></a>
                            <a href="https://playoverwatch.com/es-es/" target="_blank"><span>Overwatch</span></a>
                            <a href="https://www.pokemon.com/es/videojuegos-pokemon/pokemon-diamante-brillante-y-pokemon-perla-reluciente/" target="_blank"><span>Pokemon</span></a>
                            <a href="https://www.ubisoft.com/es-es/game/rainbow-six/siege" target="_blank"><span>Rainbow Six</span></a>
                            <a href="https://www.rocketleague.com/es-es/" target="_blank"><span>Rocket League</span></a>
                            <a href="https://www.smashbros.com/es_ES/" target="_blank"><span>Super Smash Bros</span></a>
                            <a href="https://playvalorant.com/es-es/" target="_blank"><span>Valorant</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright-area">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="ca-text">
                          <p>
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved
                          </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="ca-links">
                            <a href="/contacto">Contacto</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->
    <!-- Js Plugins -->
    <script>
      function ocultar() {
          document.getElementById('modalidad').classList.add('hide');
      }
      function mostrar() {
          document.getElementById('modalidad').classList.remove('hide');
      }
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('js/circle-progress.min.js')}}"></script>
    <script src="{{asset('js/jquery.barfiller.js')}}"></script>
    <script src="{{asset('js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
</body>

</html>
