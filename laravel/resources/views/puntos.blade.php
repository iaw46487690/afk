@extends((Request::wantsJson()) ? 'layouts.ajax' : 'layouts.app')

@section('title')
    <title>Puntos</title>
@endsection

@section('content')
    <section class="breadcrumb-section set-bg spad" data-setbg="{{ asset('img/fondoPrincipal.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb-text">
                        <br>
                        <h3>Canjear Puntos</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="categories-grid-section spad">
        <div class="container">
            <div class="section-title3">
                <h5>Puntos disponibles: {{ $info->points ?? '0' }}&nbsp;&nbsp;<span class="fa fa-product-hunt"></span>
                </h5>
            </div>
            <div class="row">
                @forelse ($prizes as $prize)
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="cg-item2">
                            <div class="cg-pic set-bg" data-setbg="{{ asset('img/prizes/') . '/' . $prize->img }}"
                                style="background-size: cover;"></div>
                            <div class="cg-text">
                                <h5><a>{{ $prize->name }}</a></h5>
                                <p>
                                    @if (app('request')->session()->get('Usuario'))
                                        @if ($info->points >= $prize->points)
                                            <a href="/puntos/{{ $prize->id }}">
                                                <button type="submit" class="btn btn-primary cg-button2"
                                                    name="button">{{ $prize->points }}&nbsp;&nbsp;<span
                                                        class="fa fa-product-hunt"></span></button>
                                            </a>
                                        @else
                                            <a class="errorPoints-switch errorPoints-open" style="cursor:pointer">
                                                <button type="submit" class="btn btn-primary cg-button2"
                                                    name="button">{{ $prize->points }}&nbsp;&nbsp;<span
                                                        class="fa fa-product-hunt"></span></button>
                                            </a>
                                        @endif
                                    @else
                                        <a href="/login" style="cursor:pointer">
                                            <button type="submit" class="btn btn-primary cg-button2"
                                                name="button">{{ $prize->points }}&nbsp;&nbsp;<span
                                                    class="fa fa-product-hunt"></span></button>
                                        </a>
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="col-12">
                        <h4 class="username">No hay premios disponibles en este momento</h4>
                    </div>
                @endforelse
            </div>
        </div>
    </section>
    <!-- Error Points Section Begin -->
    @if (app('request')->session()->get('Usuario'))
        <div id="errorPoints-section" class="errorPoints-section">
            <div class="errorPoints-text">
                <div class="container">
                    <div class="errorPoints-title">
                        <h2 style="color:#c20000">No tienes puntos suficientes</h2><br>
                        <h5 class="username">Puntos actuales: {{ $info->points }}&nbsp;&nbsp;<span class="fa fa-product-hunt"></span></h5>
                    </div>
                    <p style="font-size:18px">Puedes conseguir más puntos jugando torneos</p>
                    <div class="errorPoints-close return"><i class="fa fa-arrow-left"></i>&nbsp;<b>Volver</b></div>
                </div>
            </div>
        </div>
    @endif
    <!-- Error Points Section End -->
@endsection
