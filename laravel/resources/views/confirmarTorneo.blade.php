@extends((Request::wantsJson()) ? 'layouts.ajax' : 'layouts.app')

@section('title')
    <title>Crear Torneo</title>
@endsection

@section('content')
    <section class="details-post-section spad">
        <div class="create-text">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title3">
                            <h6>El torneo se ha creado correctamente!</h6><br><br>
                            <a href="/misTorneos">
                                <button class="btn btn-primary cg-button4"><b>IR A</b><br><span class="fa fa-list-ul">&nbsp;&nbsp;<b>MIS TORNEOS</b></span></button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
