@extends((Request::wantsJson()) ? 'layouts.ajax' : 'layouts.app')

@section('title')
    <title>Puntos</title>
@endsection

@section('content')
    <section class="details-post-section spad">
        <div class="create-text">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title3">
                            <h6>Has canjeado los puntos correctamente, tu pedido esta en camino!</h6><br><br>
                            <a href="/">
                                <button class="btn btn-primary cg-button4"><b>VOLVER A</b><br><span class="fa fa-home">&nbsp;&nbsp;<b>INICIO</b></span></button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
