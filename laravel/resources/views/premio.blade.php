@extends((Request::wantsJson()) ? 'layouts.ajax' : 'layouts.app')

@section('title')
    <title>Puntos</title>
@endsection

@section('content')
    <section class="details-post-section spad">
        <div class="create-text">
            <div class="container">
                <form action="/recibo" method="post" class="create-form">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="section-title3">
                                <h5>Puntos disponibles: {{ $info->points }}&nbsp;&nbsp;<span
                                        class="fa fa-product-hunt"></span>
                                </h5>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" style="top: -55px">
                            <img width="100%" style="margin-bottom: 25px"
                                src="{{ asset('img/prizes/') . '/' . $prize->img }}">
                            <div class="separadorCrearTorneo"></div><br>
                            <div class="section-title">
                                <div class="filtros2">
                                    <b>{{ $prize->name }}</b><br><br>
                                    Precio: {{ $prize->points }}&nbsp;&nbsp;<span
                                        class="fa fa-product-hunt"></span><br><br>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                            <div class="separadorCrearTorneo"></div><br><br>
                            <div class="sidebar-option">
                                <div class="section-title4">
                                    <h6>Datos de envio</h6>
                                </div>
                                @if ($errors->any())
                                    <div class="alert" style="color:#c20000; font-size:16px">
                                        @foreach ($errors->all() as $error)
                                            {{ $error }} <br>
                                        @endforeach
                                        <br>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="sf-input-list">
                                            <input name="nombre" type="text" class="input-value" style="margin-bottom: 25px"
                                                placeholder="Nombre">
                                        </div>
                                        <div class="sf-input-list">
                                            <input name="apellidos" type="text" class="input-value"
                                                style="margin-bottom: 25px" placeholder="Apellidos">
                                        </div>
                                        <div class="sf-input-list">
                                            <input name="email" type="text" class="input-value" style="margin-bottom: 25px"
                                                placeholder="Correo electronico">
                                        </div>
                                        <div class="sf-input-list">
                                            <input name="telefono" type="text" class="input-value"
                                                style="margin-bottom: 25px" placeholder="Telefono">
                                        </div>
                                        <div class="sf-input-list">
                                            <input name="pais" type="select" class="input-value" style="margin-bottom: 25px"
                                                placeholder="Pais">
                                        </div>
                                        <div class="sf-input-list">
                                            <input name="ciudad" type="text" class="input-value" style="margin-bottom: 25px"
                                                placeholder="Ciudad">
                                        </div>
                                        <div class="sf-input-list">
                                            <input name="codigoPostal" type="text" class="input-value"
                                                style="margin-bottom: 25px" placeholder="Codigo postal">
                                        </div>
                                        <div class="sf-input-list">
                                            <input name="direccion" type="text" class="input-value"
                                                style="margin-bottom: 25px" placeholder="Direccion">
                                        </div>
                                        <input name="points" type="hidden" value="{{ $prize->points }}">
                                        <br><button type="submit"><span>Canjear Puntos</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
