@extends((Request::wantsJson()) ? 'layouts.ajax' : 'layouts.app')

@section('title')
@if (!Request::wantsJson())
    <title>{{ $gameInfo->name ?? 'Torneos' }}</title>
@endif
@endsection

@section('content')
@if (!Request::wantsJson())
    <section class="breadcrumb-section set-bg spad" data-setbg="{{ asset('img/fondoPrincipal.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb-text">
                        <br>
                        @if (isset($search) && strlen($search) != 0)
                            <h3>Estas buscando: "{{ $search }}"</h3>
                        @else
                            <h3>{{ $gameInfo->name ?? 'Torneo'}}</h3>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="categories-grid-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12 p-0">
                    <div class="sidebar-option">
                        <div class="section-title">
                            <h5>Filtros</h5>
                            <div class="filtros">
                                <form id="filterForm" action={{ url('/juegos/filter') }} method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="col-lg-12 col-md-6">
                                            <br>
                                            <h6><b>Torneo Oficial:</b></h6>
                                            <br><b>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="official"
                                                    value="1{{ old('official') }}">&nbsp;&nbsp;Si
                                                <br>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="noOfficial"
                                                    value="0{{ old('noOfficial') }}">&nbsp;&nbsp;No</b><br>
                                        </div>
                                        <div class="col-lg-12 col-md-6">
                                            <br>
                                            <h6><b>Plataforma:</b></h6>
                                            <br><b>&nbsp;&nbsp;&nbsp;&nbsp;<input id="PlayStation" name="PlayStation"
                                                    type="checkbox" name="plataforma" value="PlayStation{{ old('PlayStation') }}">&nbsp;&nbsp;<span
                                                    class="iconify" data-icon="cib:playstation"
                                                    data-inline="false"></span>&nbsp; - &nbsp;PlayStation
                                                <br>&nbsp;&nbsp;&nbsp;&nbsp;<input id="XBOX" name="XBOX" type="checkbox"
                                                    name="plataforma" value="XBOX{{ old('XBOX') }}">&nbsp;&nbsp;<span class="iconify"
                                                    data-icon="cib:xbox" data-inline="false"></span>&nbsp; - &nbsp;XBOX
                                                <br>&nbsp;&nbsp;&nbsp;&nbsp;<input id="PC" name="PC" type="checkbox"
                                                    name="plataforma" value="PC{{ old('PC') }}">&nbsp;&nbsp;<span class="iconify"
                                                    data-icon="ls:pc" data-inline="false"></span>&nbsp; - &nbsp;PC
                                                <br>&nbsp;&nbsp;&nbsp;&nbsp;<input id="NintendoSwitch"
                                                    name="NintendoSwitch" type="checkbox" name="plataforma"
                                                    value="Nintendo Switch{{ old('NintendoSwitch') }}">&nbsp;&nbsp;<span class="iconify"
                                                    data-icon="cib:nintendo-switch" data-inline="false"></span>&nbsp; -
                                                &nbsp;Nintendo Switch
                                                <br>&nbsp;&nbsp;&nbsp;&nbsp;<input id="Movil" name="Movil" type="checkbox"
                                                    name="plataforma" value="Movil{{ old('Movil') }}">&nbsp;&nbsp;<span class="iconify"
                                                    data-icon="cil:screen-smartphone" data-inline="false"></span>&nbsp; -
                                                &nbsp;Movil
                                            </b><br><br>
                                        </div>
                                        <input type="hidden" name="search" value="{{$search ?? ''}}">
                                        <input type="hidden" name="game_id" value="{{$game ?? ''}}">
                                        <button type="submit" class="btn btn-primary cg-buttonFilters">Buscar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    $('#filterForm').submit(function(e) {
                        e.preventDefault()
                        //Datos del formulario lo guardamos como json en data
                        var data = $("#filterForm").serialize()
                        axios.post('/juegos/filter', data)
                            .then(response => {
                                console.log(response.data)
                                //La respuesta obtenida de la petición Ajax la pegamos en nuestra web
                                //substituyendo el content actual (no el menu de filtros)
                                $('#games').replaceWith(response.data)
                            })
                    })
                </script>
@endif
                <div id="games" class="col-lg-8 col-md-12 col-sm-12 p-0">
                    <div class="row">
                        @forelse ($tournaments as $tournament)
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="cg-item">
                                    <div class="cg-pic set-bg" style="background-size: cover;">
                                        <img class="img" src="{{asset('img/games/') . '/' . $tournament->img}}">
                                        <div class="label"><span>{{ $tournament->platform }}</span></div>
                                        @if ($tournament->official)
                                            <div class="label2"><span>Oficial</span></div>
                                        @endif
                                    </div>
                                    <div class="cg-text">
                                        <h5><a>{{ $tournament->name }}</a></h5>
                                        <p class="participantes">{{ $tournament->players }}/{{ $tournament->capacity }}
                                        </p>
                                        <h6 style="color: white">{{ $tournament->game }}</h6><br>
                                        <p>
                                        <ul>
                                            <li>
                                                @php
                                                    $date = $tournament->date;
                                                    $hour = substr($date,11,5);
                                                    $day = substr($date,8,2) . ' / ' . substr($date,5,2) . ' / ' . substr($date,0,4);
                                                @endphp
                                                <i class="fa fa-clock-o"></i> {{ $hour }}<br>
                                                <i class="fa fa-calendar"></i> {{ $day }}
                                            </li>
                                        </ul>
                                        @if (app('request')->session()->get('Usuario'))
                                            <a href="/torneo/{{$tournament->id}}">
                                                <button type="button" class="btn btn-primary cg-button" name="button">Ver Torneo</button>
                                            </a>
                                        @else
                                            <a href="/login" style="cursor:pointer">
                                                <button type="button" class="btn btn-primary cg-button" name="button">Ver Torneo</button>
                                            </a>
                                        @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="col-lg-8 col-md-12 col-sm-12 p-0 ">
                                <h4 class="username">No hay torneos disponibles</h4>
                            </div>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
