@extends((Request::wantsJson()) ? 'layouts.ajax' : 'layouts.app')

@section('title')
@if (!Request::wantsJson())
    <title>Inicio</title>
@endif
@endsection

@section('content')
@if (!Request::wantsJson())
    <!-- Hero Section Begin -->
    <section class="hero-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="hs-text">
                        <div class="label" style="background-color: blue"><span><b>Playstation</b></span></div>
                        <div class="label" style="background-color: green"><span><b>Xbox</b></span></div>
                        <div class="label" style="background-color: red"><span><b>Nintendo Switch</b></span></div>
                        <div class="label" style="background-color: black"><span><b>Pc</b></span></div>
                        <div class="label" style="background-color: rgb(204, 0, 255)"><span><b>Movil</b></span></div>
                        <h3>AFK, la web de torneos donde conseguir premios es posible</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="hero-slider owl-carousel">
            <img class="hs-item" src="{{ asset('img/fondoPrincipal.jpg') }}">
        </div>
    </section>
    <!-- Hero Section End -->

    <!-- Latest Preview Section Begin -->
    <section class="latest-preview-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h5>Proximos Torneos</h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="lp-slider owl-carousel">
                    @foreach ($tournaments as $tournament)
                        <div class="col-lg-3">
                            <div class="cg-item">
                                <div class="cg-picPrincipal">
                                    <div class="label"><span><b>{{ $tournament->platform }}</b></span></div>
                                    @if ($tournament->official)
                                        <div class="label2"><span><b>Oficial</b></span></div>
                                    @endif
                                    <img src="{{ asset('img/games/') . '/' . $tournament->img }}"><br>
                                    <div class="cg-text" style="padding-top: 0px;">
                                        <h5><a href="#">{{ $tournament->name }}</a></h5>
                                        <p class="participantes">{{ $tournament->players }}/{{ $tournament->capacity }}
                                        </p>
                                        <h6 style="color: white">{{ $tournament->game }}</h6><br>
                                        <p>
                                        <ul>
                                            <li>
                                                @php
                                                    $date = $tournament->date;
                                                    $hour = substr($date, 11, 5);
                                                    $day = substr($date, 8, 2) . ' / ' . substr($date, 5, 2) . ' / ' . substr($date, 0, 4);
                                                @endphp
                                                <span><i class="fa fa-clock-o"></i>{{ $hour }}</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <span><i class="fa fa-calendar"></i>{{ $day }}</span>
                                            </li>
                                        </ul>
                                        @if (app('request')->session()->get('Usuario'))
                                            <a href="/torneo/{{ $tournament->id }}">
                                                <button type="button" class="btn btn-primary cg-button3" name="button">Ver Torneo</button>
                                            </a>
                                        @else
                                            <a href="/login" style="cursor:pointer">
                                                <button type="button" class="btn btn-primary cg-button3" name="button">Ver Torneo</button>
                                            </a>
                                        @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- Latest Preview Section End -->

    <!-- Update News Section Begin -->
    <section class="update-news-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h5><span>Torneos x Plataforma</span></h5><br>
                    </div>
                    <button class="selectPlatform btn btn-primary cg-buttontxp" name="Todos" style="margin:3px; font-size:16px">Todos</button>&nbsp;
                    @forelse ($platforms as $platform)
                        <button class="selectPlatform btn btn-primary cg-buttontxp" name="{{$platform->platform}}" style="margin:3px; font-size:16px">{{$platform->platform}}</button>&nbsp;
                    @empty

                    @endforelse
                        <br><br>
                    <script>
                        $('.selectPlatform').click(function(e) {
                            e.preventDefault()
                            var platform = $(this).attr("name")
                            console.log(platform)
                            var data = $(".selectPlatform").serialize()
                            axios.post('/inicio/'+platform, data)
                                .then(response => {
                                    console.log(response.data)
                                    $('#myCarousel').replaceWith(response.data)
                                }
                            );
                        });
                    </script>
@endif
                    <div id="myCarousel" class="myCarousel carousel slide" data-interval="false">
                        <!-- carrousel -->
                        <div class="item active" style="padding-top:50px">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        @forelse ($tournamentsPlatform as $tournament)
                                            <div class="col-lg-4 col-md-6" style="padding-bottom: 15px">
                                                <div class="cg-item">
                                                    <div class="cg-picPrincipal">
                                                        <div class="label">
                                                            <span><b>{{ $tournament->platform }}</b></span>
                                                        </div>
                                                        @if ($tournament->official)
                                                            <div class="label2"><span><b>Oficial</b></span></div>
                                                        @endif
                                                        <img src="{{ asset('img/games/') . '/' . $tournament->img }}" style="max-width:100%"><br>
                                                        <div class="cg-text" style="padding-top: 0px;">
                                                            <h5><a href="#">{{ $tournament->name }}</a></h5>
                                                            <p class="participantes">
                                                                {{ $tournament->players }}/{{ $tournament->capacity }}
                                                            </p>
                                                            <h6 style="color: white">{{ $tournament->game }}</h6>
                                                            <br>
                                                            <p>
                                                            <ul>
                                                                <li>
                                                                    @php
                                                                        $date = $tournament->date;
                                                                        $hour = substr($date, 11, 5);
                                                                        $day = substr($date, 8, 2) . ' / ' . substr($date, 5, 2) . ' / ' . substr($date, 0, 4);
                                                                    @endphp
                                                                    <span><i class="fa fa-clock-o"></i>{{ $hour }}</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    <span><i class="fa fa-calendar"></i>{{ $day }}</span>
                                                                </li>
                                                            </ul>
                                                            @if (app('request')->session()->get('Usuario'))
                                                                <a href="/torneo/{{ $tournament->id }}">
                                                                    <button type="button" class="btn btn-primary cg-button3" name="button">Ver Torneo</button>
                                                                </a>
                                                            @else
                                                                <a href="/login" style="cursor:pointer">
                                                                    <button type="button" class="btn btn-primary cg-button3" name="button">Ver Torneo</button>
                                                                </a>
                                                            @endif
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div><br><br>
                                            </div>
                                        @empty
                                            <div class="col-lg-8 col-md-12 col-sm-12 p-0 ">
                                                <h4 class="username">No hay torneos disponibles para esta plataforma</h4>
                                                <br><br><br><br>
                                            </div>
                                        @endforelse
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Update News Section End -->

    @if (!Request::wantsJson())
    <!-- Videos Guide Section Begin -->
    <section class="video-guide-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h5>Trailers de los videojuegos</h5>
                    </div>
                </div>
            </div>
            <div class="tab-elem">
                {{-- Tab panes --}}
                <div class="tab-content">
                    <div class="row">
                        <div id="myCarouselVideo" class="carousel slide" data-interval="false">
                            {{-- Wrapper for slides --}}
                            <div class="carousel-inner">
                                <div class="item active">
                                    <div class="col-lg-12 text-center">
                                        <h4 class="username text-center">{{ $gameFilters[0]->name }}</h4><br>
                                        <div class="row">
                                            <div class="col-12">
                                                <iframe width="1243" height="699" src="https://www.youtube.com/embed/{{'/' . $gameFilters[0]->video}}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--@for ($i = 1; $i < count($gameFilters); $i++)
                                    <div class="item">
                                        <div class="col-lg-12">
                                            <h4 class="text-center username">{{ $gameFilters[$i]->name }}</h4><br>
                                            <div class="row">
                                                <div class="col-12">
                                                    <iframe width="1243" height="699" src="https://www.youtube.com/embed/{{'/' . $gameFilters[$i]->video}}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                    {{-- <video width="100%" height="100%" controls>
                                                        <source
                                                            src="{{ asset('img/videos/') . '/' . $gameFilters[$i]->video }}"
                                                            type="video/mp4">
                                                    </video>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endfor--}}
                            </div>
                            {{-- Controls --}}
                            <a class="left carousel-control" style="width: 0%; height:20%; top:-8%; left: 7%"
                                href="#myCarouselVideo" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" style="width: 0%; height:20%; top:-8%; right: 7%"
                                href="#myCarouselVideo" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
    <!-- Videos Guide Section End -->
@endsection
