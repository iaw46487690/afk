@extends((Request::wantsJson()) ? 'layouts.ajax' : 'layouts.app')

@section('title')
    <title>Configuración</title>
@endsection

@section('content')
    <!-- Configuración Begin -->
    <div class="conf-text" style="background-color: #0d0d0d">
        <div class="container">
            <div class="conf-title">
                <h2>Configuración</h2>
            </div>
            <form action="/configuracion" class="conf-form" method="POST" enctype="multipart/form-data">
                @csrf
                @if ($errors->any())
                    <div class="alert" style="color:#c20000; font-size:16px">
                        @foreach ($errors->all() as $error)
                            {{ $error }} <br>
                        @endforeach
                        <br>
                    </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <h4 class="username">Mis Datos</h4><br>
                        <div class="sf-input-list">
                            <input type="text" class="input-value" name="name" placeholder="Nombre de usuario" value="{{ $info->name }}">
                            <input type="password" class="input-value" name="password" placeholder="Contraseña">
                            <input type="password" class="input-value" name="password_confirmation"
                                placeholder="Repetir Contraseña">
                        </div>
                    </div>
                    <div class="col-12">
                        <br>
                        <h4 class="username">Redes Sociales</h4><br>
                        <div class="sf-input-list" style="font-size: 16px">
                            <i class="fa fa-youtube-play username"></i>&nbsp;&nbsp;<input type="text" class="input-value"
                                name="youtube" placeholder="Link Youtube" value="{{ $info->youtube }}"><br>
                            <i class="fa fa-twitch username"></i>&nbsp;&nbsp;<input type="text" class="input-value"
                                name="twitch" placeholder="Link Twitch" value="{{ $info->twitch }}"><br>
                            <i class="fa fa-twitter username"></i>&nbsp;&nbsp;<input type="text" class="input-value"
                                name="twitter" placeholder="Link Twitter" value="{{ $info->twitter }}"><br>
                            <i class="fa fa-instagram username"></i>&nbsp;&nbsp;<input type="text" class="input-value"
                                name="instagram" placeholder="Link Instagram" value="{{ $info->instagram }}"><br>
                        </div>
                    </div>
                    <div class="col-12">
                        <br>
                        <h4 class="username">Plataformas</h4><br>
                        <div class="sf-input-list" style="font-size: 16px">
                            <span class="iconify username" data-icon="mdi-sony-playstation"
                                data-inline="false"></span>&nbsp;&nbsp;
                            <input type="text" class="input-value" name="idps" placeholder="Id PlayStation"
                                value="{{ $info->idps }}"><br>
                            <span class="iconify username" data-icon="cib:xbox" data-inline="false"></span>&nbsp;&nbsp;
                            <input type="text" class="input-value" name="idxbox" placeholder="Id Xbox"
                                value="{{ $info->idxbox }}"><br>
                            <span class="iconify username" data-icon="simple-icons:epicgames"
                                data-inline="false"></span>&nbsp;&nbsp;
                            <input type="text" class="input-value" name="idepicgames" placeholder="Id Epic Games"
                                value="{{ $info->idepicgames }}"><br>
                            <span class="iconify username" data-icon="cib:nintendo-switch"
                                data-inline="false"></span>&nbsp;&nbsp;
                            <input type="text" class="input-value" name="idswitch" placeholder="Id Nintendo-Switch"
                                value="{{ $info->idswitch }}"><br>
                            <span class="iconify username" data-icon="cib:steam" data-inline="false"></span>&nbsp;&nbsp;
                            <input type="text" class="input-value" name="idsteam" placeholder="Id Steam"
                                value="{{ $info->idsteam }}"><br>
                            <span class="iconify username" data-icon="simple-icons:riotgames"
                                data-inline="false"></span>&nbsp;&nbsp;
                            <input type="text" class="input-value" name="idriotgames" placeholder="Id Riot Games"
                                value="{{ $info->idriotgames }}"><br>
                            <span class="iconify username" data-icon="fa-brands:battle-net"
                                data-inline="false"></span>&nbsp;&nbsp;
                            <input type="text" class="input-value" name="idblizzard" placeholder="Id Battle.net"
                                value="{{ $info->idblizzard }}"><br>
                            <span class="iconify username" data-icon="la:stripe-s" data-inline="false"
                                data-inline="false"></span>&nbsp;&nbsp;
                            <input type="text" class="input-value" name="idsupercell" placeholder="Id Supercell"
                                value="{{ $info->idsupercell }}"><br>
                        </div>
                    </div>
                    <div class="col-12">
                        <br>
                        <h4 class="username">Imagenes</h4><br>
                        <div class="sf-input-list username">
                            <p>Imagen de Perfil:</p>
                            <input type="file" class="btn btn-primary" name="img" id="img">
                        </div>
                    </div>
                    <div class="col-12">
                        <p>Imagen de Fondo:</p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <input type="radio" name="bannerPerfil" value="comecocos.png">&nbsp;&nbsp;<img width="150px"
                            height="50px" src="{{ asset('img/bannerProfile/comecocos.png') }}"><br><br>
                        <input type="radio" name="bannerPerfil" value="gameOver.jpg">&nbsp;&nbsp;<img width="150px"
                            height="50px" src="{{ asset('img/bannerProfile/gameOver.jpg') }}"><br><br>
                        <input type="radio" name="bannerPerfil" value="life.webp">&nbsp;&nbsp;<img width="150px"
                            height="50px" src="{{ asset('img/bannerProfile/life.webp') }}"><br><br>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <input type="radio" name="bannerPerfil" value="mario.webp">&nbsp;&nbsp;<img width="150px"
                            height="50px" src="{{ asset('img/bannerProfile/mario.webp') }}"><br><br>
                        <input type="radio" name="bannerPerfil" value="skyrim.webp">&nbsp;&nbsp;<img width="150px"
                            height="50px" src="{{ asset('img/bannerProfile/skyrim.webp') }}"><br><br>
                        <input type="radio" name="bannerPerfil" value="tetris.webp">&nbsp;&nbsp;<img width="150px"
                            height="50px" src="{{ asset('img/bannerProfile/tetris.webp') }}"><br><br>
                    </div>
                </div>
                <br><button type="submit"><span>Guardar Cambios</span></button>
            </form>
        </div>
    </div>
    <!-- Configuración End -->
@endsection
