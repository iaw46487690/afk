@extends((Request::wantsJson()) ? 'layouts.ajax' : 'layouts.app')

@section('title')
    <title>Contacto</title>
@endsection

@section('content')
    <section class="breadcrumb-section set-bg spad" data-setbg="{{asset('img/fondoPrincipal.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb-text">
                        <br><h3>Contacto</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="contact-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="contact-text">
                        <div class="contact-title">
                            <h3>Ayudanos a mejorar</h3>
                            <p>Si tienes cualquier consulta o sugerencia, no dudes en contactar con nosotros,
                                estaremos encantados de poder mejorar la experiencia para todos los usuarios</p>
                        </div>
                        <div class="contact-form">
                            <div class="dt-leave-comment">
                                <form action="#">
                                    <div class="input-list">
                                        <input type="text" placeholder="Nombre">
                                        <input type="text" placeholder="Correo">
                                    </div>
                                    <textarea placeholder="Mensaje"></textarea>
                                    <button type="submit">Enviar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
