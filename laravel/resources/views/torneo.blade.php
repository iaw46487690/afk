@extends((Request::wantsJson()) ? 'layouts.ajax' : 'layouts.app')

@section('title')
    <title>Torneo</title>
@endsection

@section('content')
    <section class="details-post-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sidebar-option">
                        <div class="section-title">
                            <div class="filtros2">
                                <div class="row">
                                    <div class="col-8">
                                        <h4 class="username"><b>{{ $tournament->name }}</b></h4><br><br>
                                    </div>
                                    <div class="col-4">
                                        @if ($tournament->official)
                                            <div class="label3"><span>Torneo Oficial</span></div>
                                        @endif
                                    </div>
                                    <div class="col-6">
                                        Juego: {{ $tournament->game }}<br><br>
                                        Plataforma: {{ $tournament->platform }}<br><br>
                                    </div>
                                    <div class="col-6">
                                        Jugadores: {{ $tournament->players }}/{{ $tournament->capacity }}<br><br>
                                        @php
                                            $date = $tournament->date;
                                            $hour = substr($date, 11, 5);
                                            $day = substr($date, 8, 2) . ' / ' . substr($date, 5, 2) . ' / ' . substr($date, 0, 4);
                                        @endphp
                                        Fecha de inicio:<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<i
                                            class="fa fa-calendar"></i>&nbsp;&nbsp;{{ $day }}<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<i
                                            class="fa fa-clock-o"></i>&nbsp;&nbsp;{{ $hour }}<br><br>
                                    </div>
                                    <div class="col-12 div-inscribirse">
                                        @if (app('request')->session()->get('Usuario')['type'] == 'estandar')
                                            @if ($expiredOrPlaying == 'nothing')
                                                <a href="/registro/{{ $tournament->id }}">
                                                    <button type="button" class="btn btn-primary button-inscribirse"
                                                        name="button">Inscribete!</button>
                                                </a>
                                            @elseif ($expiredOrPlaying == 'expired')
                                                <h4 class="username">Este torneo ha expirado</h4>
                                            @else
                                                <a href="/salir/{{ $tournament->id }}">
                                                    <button type="button" class="btn btn-primary button-inscribirse"
                                                        name="button">Desinscribete</button>
                                                </a>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="todo">
            @if ($expiredOrPlaying != 'expired')
                <h4 class="username">Los emparejamientos estaran disponibles cuando empiece el torneo</h4>
            @else
                <div class="cuadroTorneo2">
                    <div class="cabecera" id="myHeader">
                        @if ($tournament->capacity >= 64)
                            <div class="rondaCabecera">Ronda 1</div>
                            @php
                                $i = 2;
                            @endphp
                        @endif
                        @if ($tournament->capacity >= 32)
                            <div class="rondaCabecera">Ronda {{ $i ?? '1' }}</div>
                        @endif
                        @if ($tournament->capacity >= 16)
                            <div class="rondaCabecera">Octavos</div>
                        @endif
                        <div class="rondaCabecera">Cuartos</div>
                        <div class="rondaCabecera">Semifinales</div>
                        <div class="rondaCabecera">Final</div>
                    </div>
                    <div class="enfrentamientos">
                        @php
                            $ronda = $tournament->capacity;
                        @endphp
                        @foreach ($matches as $match)
                            @php
                                $ronda = $ronda / 2;
                            @endphp
                            <div class="ronda ronda{{ $ronda }}">

                                @if (count($match) != 0 && count($match) == $ronda * 2)
                                    @for ($i = 0; $i < count($match); $i++)
                                        <div class="partido">
                                            @if (!is_null($match[$i]->username))
                                                @if (is_null($match[$i]->winner))
                                                    <div class="participante">
                                                        @if ($userInfo->id == $match[$i]->user_id)
                                                            <a href="/looser/{{$match[$i]->tournament_id}}" class="username">
                                                                <span class="iconify icons" data-icon="feather:x"
                                                                data-inline="false"></span>
                                                            </a>
                                                            <a href="/winner/{{$match[$i]->tournament_id}}" class="username">
                                                                <span class="iconify icons" data-icon="feather:check"
                                                                data-inline="false"></span>
                                                            </a>
                                                        @endif
                                                        <a href="/perfil/{{ $match[$i]->user_id }}" target="_blank"
                                                            class="username">
                                                            {{ $match[$i]->username ?? '- - -' }}
                                                        </a>
                                                    </div>
                                                @elseif ($match[$i]->winner == true)
                                                    <div class="participante">
                                                        <a href="/perfil/{{ $match[$i]->user_id }}" target="_blank"
                                                            class="username">
                                                            {{ $match[$i]->username ?? '- - -' }}
                                                        </a>
                                                    </div>
                                                @else
                                                    <div class="looser-top">
                                                        <a href="/perfil/{{ $match[$i]->user_id }}" target="_blank"
                                                            class="username">
                                                            {{ $match[$i]->username ?? '- - -' }}
                                                        </a>
                                                    </div>
                                                @endif
                                            @else
                                                @if (is_null($match[$i]->winner))
                                                    <div class="participante">
                                                        {{ $match[$i]->username ?? '- - -' }}
                                                    </div>
                                                @elseif ($match[$i]->winner == true)
                                                    <div class="participante">
                                                        {{ $match[$i]->username ?? '- - -' }}
                                                    </div>
                                                @else
                                                    <div class="looser-top">
                                                        {{ $match[$i]->username ?? '- - -' }}
                                                    </div>
                                                @endif
                                            @endif
                                            <div class="separacion"></div>
                                            @php
                                                $i++;
                                            @endphp
                                            @if (!is_null($match[$i]->username))
                                                @if (is_null($match[$i]->winner))
                                                    <div class="participante">
                                                        @if ($userInfo->id == $match[$i]->user_id)
                                                        <a href="/looser/{{$match[$i]->tournament_id}}" class="username">
                                                                <span class="iconify icons" data-icon="feather:x"
                                                                data-inline="false"></span>
                                                            </a>
                                                            <a href="/winner/{{$match[$i]->tournament_id}}" class="username">
                                                                <span class="iconify icons" data-icon="feather:check"
                                                                data-inline="false"></span>
                                                            </a>
                                                        @endif
                                                        <a href="/perfil/{{ $match[$i]->user_id }}" target="_blank"
                                                            class="username">
                                                            {{ $match[$i]->username ?? '- - -' }}
                                                        </a>
                                                    </div>
                                                @elseif ($match[$i]->winner == true)
                                                    <div class="participante">
                                                        <a href="/perfil/{{ $match[$i]->user_id }}" target="_blank"
                                                            class="username">
                                                            {{ $match[$i]->username ?? '- - -' }}
                                                        </a>
                                                    </div>
                                                @else
                                                    <div class="looser-bottom">
                                                        <a href="/perfil/{{ $match[$i]->user_id }}" target="_blank"
                                                            class="username">
                                                            {{ $match[$i]->username ?? '- - -' }}
                                                        </a>
                                                    </div>
                                                @endif
                                            @else
                                                @if (is_null($match[$i]->winner))
                                                    <div class="participante">
                                                        {{ $match[$i]->username ?? '- - -' }}
                                                    </div>
                                                @elseif ($match[$i]->winner == true)
                                                    <div class="participante">
                                                        {{ $match[$i]->username ?? '- - -' }}
                                                    </div>
                                                @else
                                                    <div class="looser-bottom">
                                                        {{ $match[$i]->username ?? '- - -' }}
                                                    </div>
                                                @endif
                                            @endif
                                        </div>
                                    @endfor
                                @else
                                    @for ($i = 0; $i < $ronda; $i++)
                                        <div class="partido">
                                            <div class="participante">&nbsp;</div>
                                            <div class="separacion"></div>
                                            <div class="participante">&nbsp;</div>
                                        </div>
                                    @endfor
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
    </section>
@endsection
