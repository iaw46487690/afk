@extends((Request::wantsJson()) ? 'layouts.ajax' : 'layouts.app')

@section('title')
    <title>Perfil</title>
@endsection
@section('content')
    @if (isset($infoUser))
        @php
            $info = $infoUser;
        @endphp
    @endif
    <section class="">
        <img class="details-hero-sectionProfile" src="{{asset('img/bannerProfile/')."/".$info->backImg}}">
    </section>
    <section class="details-post-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center">
                    <img class="imgPerfil" src="{{asset('img/profileImg/')."/".$info->img}}" alt="">
                </div>
                <div class="col-lg-6 col-md-5 col-sm-8 col-xs-6 nombrePerfil">
                    <h2 class="username">{{ $info->name }}</h2>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 text-center editarPerfil">
                    @if (!isset($infoUser))
                        <a href="/configuracion">
                            <button class="btn conf-switch conf-open" style="font-size: 16px;">
                                <span class="fa fa-cog">&nbsp;&nbsp;Editar Perfil</span>
                            </button>
                        </a>
                    @endif
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-8 p-0">
                    <div class="col-lg-12 p-0">
                        <div class="details-text">
                            <div class="dt-overall-rating">
                                <div class="or-heading idPlataforma" style="font-size: 25px">
                                    <div class="divPlataforma">
                                        <div class="or-item">
                                            <span class="iconify" data-icon="mdi-sony-playstation"
                                                data-inline="false"></span>&nbsp;&nbsp;{{$info->idps ?? '_________'}}&nbsp;&nbsp;
                                        </div>
                                        <div class="or-item">
                                            <span class="iconify" data-icon="cib:xbox"
                                                data-inline="false"></span>&nbsp;&nbsp;{{$info->idxbox ?? '_________'}}&nbsp;&nbsp;
                                        </div>
                                        <div class="or-item">
                                            <span class="iconify" data-icon="simple-icons:epicgames"
                                                data-inline="false"></span>&nbsp;&nbsp;{{$info->idepicgames ?? '_________'}}&nbsp;&nbsp;
                                        </div>
                                        <div class="or-item">
                                            <span class="iconify" data-icon="cib:nintendo-switch"
                                                data-inline="false"></span>&nbsp;&nbsp;{{$info->idswitch ?? '_________'}}&nbsp;&nbsp;
                                        </div>
                                    </div>
                                    <div class="divPlataforma">
                                        <div class="or-item">
                                            <span class="iconify" data-icon="cib:steam"
                                                data-inline="false"></span>&nbsp;&nbsp;{{$info->idsteam ?? '_________'}}&nbsp;&nbsp;
                                        </div>
                                        <div class="or-item">
                                            <span class="iconify" data-icon="simple-icons:riotgames"
                                                data-inline="false"></span>&nbsp;&nbsp;{{$info->idriotgames ?? '_________'}}&nbsp;&nbsp;
                                        </div>
                                        <div class="or-item">
                                            <span class="iconify" data-icon="fa-brands:battle-net"
                                                data-inline="false"></span>&nbsp;&nbsp;{{$info->idblizzard ?? '_________'}}&nbsp;&nbsp;
                                        </div>
                                        <div class="or-item">
                                            <span class="iconify" data-icon="la:stripe-s" data-inline="false"
                                                data-inline="false"></span>&nbsp;&nbsp;{{$info->idsupercell ?? '_________'}}&nbsp;&nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!---->
                    @if (!isset($infoUser))
                        <div class="col-lg-12 p-0 borderPuntos">
                            <div class="col-lg-12 p-0">
                                <div class="text-center divCirculo">
                                    <div class="puntosCirculo">
                                        <p class="puntosTexto"><b style="font-size:250%;">{{$info->points}}</b><br><br><b>PUNTOS</b></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 p-0">
                                <div class="text-center">
                                    <p>Puedes conseguir más puntos jugando torneos</p>
                                    <p>Los puntos que consigas se podrán canjear por premios</p>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <!---->
                <div class="col-lg-4 p-0">
                    <div class="col-lg-12 col-md-12">
                        <div class="sidebar-option">
                            <div class="social-media">
                                <div class="section-title">
                                    <h5>Redes Sociales</h5>
                                </div>
                                <ul>
                                    @if ($info->youtube)
                                    <a href="{{$info->youtube}}" target="_blank">
                                    @else
                                    <a href="javascript:void(0)">
                                    @endif
                                        <li class="youtube">
                                            <div class="sm-icon"><i class="fa fa-youtube-play"></i></div>
                                            <span>Youtube</span>
                                        </li>
                                    </a>
                                    @if ($info->twitch)
                                    <a href="{{$info->twitch}}" target="_blank">
                                    @else
                                    <a href="javascript:void(0)">
                                    @endif
                                        <li class="twitch">
                                            <div class="sm-icon"><i class="fa fa-twitch"></i></div>
                                            <span>Twitch</span>
                                        </li>
                                    </a>
                                    @if ($info->twitter)
                                    <a href="{{$info->twitter}}" target="_blank">
                                    @else
                                    <a href="javascript:void(0)">
                                    @endif
                                        <li class="twitter">
                                            <div class="sm-icon"><i class="fa fa-twitter"></i></div>
                                            <span>Twitter</span>
                                        </li>
                                    </a>
                                    @if ($info->instagram)
                                    <a href="{{$info->instagram}}" target="_blank">
                                    @else
                                    <a href="javascript:void(0)">
                                    @endif
                                        <li class="instagram">
                                            <div class="sm-icon"><i class="fa fa-instagram"></i></div>
                                            <span>Instagram</span>
                                        </li>
                                    </a>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
