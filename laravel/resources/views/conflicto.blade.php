@extends((Request::wantsJson()) ? 'layouts.ajax' : 'layouts.app')

@section('title')
    <title>Torneo</title>
@endsection

@section('content')
    <section class="details-post-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sidebar-option">
                        <div class="section-title">
                            <div class="filtros2">
                                <div class="row">
                                    <div class="col-8">
                                        <h4 class="username"><b>{{ $tournament->name }}</b></h4><br><br>
                                    </div>
                                    <div class="col-4">
                                        @if ($tournament->official)
                                            <div class="label3"><span>Torneo Oficial</span></div>
                                        @endif
                                    </div>
                                    <div class="col-6">
                                        Juego: {{ $tournament->game }}<br><br>
                                        Plataforma: {{ $tournament->platform }}<br><br>
                                    </div>
                                    <div class="col-6">
                                        Jugadores: {{ $tournament->players }}/{{ $tournament->capacity }}<br><br>
                                        @php
                                            $date = $tournament->date;
                                            $hour = substr($date, 11, 5);
                                            $day = substr($date, 8, 2) . ' / ' . substr($date, 5, 2) . ' / ' . substr($date, 0, 4);
                                        @endphp
                                        Fecha de inicio:<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<i
                                            class="fa fa-calendar"></i>&nbsp;&nbsp;{{ $day }}<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<i
                                            class="fa fa-clock-o"></i>&nbsp;&nbsp;{{ $hour }}<br><br>
                                    </div>
                                    <div class="col-12 div-inscribirse">
                                        @if (app('request')->session()->get('Usuario')['type'] == 'estandar')
                                            @if ($expiredOrPlaying == 'nothing')
                                                <a href="/registro/{{ $tournament->id }}">
                                                    <button type="button" class="btn btn-primary button-inscribirse"
                                                        name="button">Inscribete!</button>
                                                </a>
                                            @elseif ($expiredOrPlaying == 'expired')
                                                <h4 class="username">Este torneo ha expirado</h4>
                                            @else
                                                <a href="/salir/{{ $tournament->id }}">
                                                    <button type="button" class="btn btn-primary button-inscribirse"
                                                        name="button">Desinscribete</button>
                                                </a>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                @for ($i = 0; $i < count($conflicts); $i++)
                    <div class="col-lg-6">
                        <div class="row">
                            <h4 class="username" style="display: flex; align-items: center; width: 50px">{{ $i + 1 }}
                            </h4>
                            <h4 class="username" style="display: flex; align-items: center;">Ronda del conflicto:
                                {{ $conflicts[$i]->round }}</h4>
                        </div>
                        <div class="partido">
                            <div class="participante">
                                <a href="/perfil/{{ $conflicts[$i]->user_id_1 }}" target="_blank" class="username"
                                    style="font-size: 16px">
                                    {{ $conflicts[$i]->player1 }}
                                </a>
                                <a href="/winnerConflict/{{ $conflicts[$i]->tournament_id }}&{{ $conflicts[$i]->user_id_1 }}&{{ $conflicts[$i]->round }}" class="username">
                                    <span class="iconify icons" data-icon="feather:check"
                                    data-inline="false"></span>
                                </a>
                            </div>
                            <div class="separacion"></div>
                            <div class="participante">
                                <a href="/perfil/{{ $conflicts[$i]->user_id_2 }}" target="_blank" class="username"
                                    style="font-size: 16px">
                                    {{ $conflicts[$i]->player2 }}
                                </a>
                                <a href="/winnerConflict/{{ $conflicts[$i]->tournament_id }}&{{ $conflicts[$i]->user_id_2 }}&{{ $conflicts[$i]->round }}" class="username">
                                    <span class="iconify icons" data-icon="feather:check"
                                    data-inline="false"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                @endfor
                @if (count($conflicts) == 0)
                    <h4 class="username">No hay conflictos en este torneo</h4>
                @endif
            </div>
        </div>

    </section>
@endsection
