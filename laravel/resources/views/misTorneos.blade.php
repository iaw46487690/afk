@extends((Request::wantsJson()) ? 'layouts.ajax' : 'layouts.app')

@section('title')
    <title>Mis Torneos</title>
@endsection

@section('content')
    <section class="breadcrumb-section set-bg spad" data-setbg="{{ asset('img/fondoPrincipal.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb-text">
                        <br>
                        <h3>Mis Torneos</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="categories-grid-section spad">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title2">
                        <h5>&nbsp;&nbsp;&nbsp;Proximos Torneos</h5>
                    </div>
                    <div class="tournaments">
                        <div id="rowNext" class="row">
                            @forelse ($nextTournaments as $tournament)
                                <div class="col-3" style="min-width: 290px">
                                    <div class="cg-item">
                                        <div class="cg-pic set-bg"
                                            data-setbg="{{ asset('img/games/') . '/' . $tournament->img }}"
                                            style="background-size: cover; width: 100%; heigth: 100%">
                                            <div class="label"><span>{{ $tournament->platform }}</span></div>
                                            @if ($tournament->official)
                                                <div class="label2"><span>Oficial</span></div>
                                            @endif
                                        </div>
                                        <div class="cg-text">
                                            <h5><a>{{ $tournament->name }}</a></h5>
                                            <p class="participantes">
                                                {{ $tournament->players }}/{{ $tournament->capacity }}
                                            </p>
                                            <h6 style="color: white">{{ $tournament->game }}</h6><br>
                                            <p>
                                            <ul>
                                                <li>
                                                    @php
                                                        $date = $tournament->date;
                                                        $hour = substr($date, 11, 5);
                                                        $day = substr($date, 8, 2) . ' / ' . substr($date, 5, 2) . ' / ' . substr($date, 0, 4);
                                                    @endphp
                                                    <i class="fa fa-clock-o"></i> {{ $hour }}<br>
                                                    <i class="fa fa-calendar"></i> {{ $day }}
                                                </li>
                                            </ul>
                                            <a href="/torneo/{{ $tournament->tournament_id ?? $tournament->id }}">
                                                <button type="button" class="btn btn-primary cg-button" name="button">Ver
                                                    Torneo</button>
                                            </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <div class="col-12 p-0">
                                    @if (app('request')->session()->get('Usuario')['type'] == 'estandar')
                                        <h4 class="username">No te has inscrito a ningun torneo</h4>
                                    @else
                                        <h4 class="username">No has creado ningun torneo</h4>
                                    @endif
                                </div>
                            @endforelse
                        </div>
                    </div>
                    <br><br>
                    <div class="section-title2">
                        <h5>&nbsp;&nbsp;&nbsp;Torneos Finalizados</h5>
                    </div>
                    <div class="tournaments">
                        <div id="rowOld" class="row">
                            @forelse ($oldTournaments as $tournament)
                                <div class="col-3" style="min-width: 290px">
                                    <div class="cg-item">
                                        <div class="cg-pic set-bg"
                                            data-setbg="{{ asset('img/games/') . '/' . $tournament->img }}"
                                            style="background-size: cover; width: 100%; heigth: 100%">
                                            <div class="label"><span>{{ $tournament->platform }}</span></div>
                                            @if ($tournament->official)
                                                <div class="label2"><span>Oficial</span></div>
                                            @endif
                                        </div>
                                        <div class="cg-text">
                                            <h5><a>{{ $tournament->name }}</a></h5>
                                            <p class="participantes">
                                                {{ $tournament->players }}/{{ $tournament->capacity }}
                                            </p>
                                            <h6 style="color: white">{{ $tournament->game }}</h6><br>
                                            <p>
                                            <ul>
                                                <li>
                                                    @php
                                                        $date = $tournament->date;
                                                        $hour = substr($date, 11, 5);
                                                        $day = substr($date, 8, 2) . ' / ' . substr($date, 5, 2) . ' / ' . substr($date, 0, 4);
                                                    @endphp
                                                    <i class="fa fa-clock-o"></i> {{ $hour }}<br>
                                                    <i class="fa fa-calendar"></i> {{ $day }}
                                                </li>
                                            </ul>
                                            <a href="/torneo/{{ $tournament->tournament_id ?? $tournament->id }}">
                                                <button type="button" class="btn btn-primary cg-button" name="button">Ver
                                                    Torneo</button>
                                            </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <div class="col-12 p-0">
                                    @if (app('request')->session()->get('Usuario')['type'] == 'estandar')
                                        <h4 class="username">Aun no has participado en ningun torneo</h4>
                                    @else
                                        <h4 class="username">No has creado ningun torneo</h4>
                                    @endif
                                </div>
                            @endforelse
                        </div>
                    </div>
                    @if (app('request')->session()->get('Usuario')['type'] == 'admin')
                        <br><br>
                        <div class="section-title2">
                            <h5>&nbsp;&nbsp;&nbsp;Conflictos en torneos</h5>
                        </div>
                        <div class="tournaments">
                            <div id="rowOld" class="row">
                                @forelse ($conflictTournaments as $tournament)
                                    <div class="col-3" style="min-width: 290px">
                                        <div class="cg-item">
                                            <div class="cg-pic set-bg"
                                                data-setbg="{{ asset('img/games/') . '/' . $tournament->img }}"
                                                style="background-size: cover; width: 100%; heigth: 100%">
                                                <div class="label"><span>{{ $tournament->platform }}</span></div>
                                                @if ($tournament->official)
                                                    <div class="label2"><span>Oficial</span></div>
                                                @endif
                                            </div>
                                            <div class="cg-text">
                                                <h5><a>{{ $tournament->name }}</a></h5>
                                                <p class="participantes">
                                                    {{ $tournament->players }}/{{ $tournament->capacity }}
                                                </p>
                                                <h6 style="color: white">{{ $tournament->game }}</h6><br>
                                                <p>
                                                <ul>
                                                    <li>
                                                        @php
                                                            $date = $tournament->date;
                                                            $hour = substr($date, 11, 5);
                                                            $day = substr($date, 8, 2) . ' / ' . substr($date, 5, 2) . ' / ' . substr($date, 0, 4);
                                                        @endphp
                                                        <i class="fa fa-clock-o"></i> {{ $hour }}<br>
                                                        <i class="fa fa-calendar"></i> {{ $day }}
                                                    </li>
                                                </ul>
                                                <a href="/torneo/{{ $tournament->tournament_id ?? $tournament->id }}/conflictos">
                                                    <button type="button" class="btn btn-primary cg-button" style="width: 140px" name="button">Ver
                                                        Conflictos</button>
                                                </a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    <div class="col-12 p-0">
                                        <h4 class="username">No hay ningun conflicto en tus torneos</h4>
                                    </div>
                                @endforelse
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
